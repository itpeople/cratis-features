
from cratis.features import Feature


class Celery(Feature):
    def configure_settings(self):
        super(Celery, self).configure_settings()

        self.append_apps([
            "djcelery",
            # "celery_admin",
        ])

        self.settings.CELERY_RESULT_BACKEND = 'djcelery.backends.database:DatabaseBackend'
        self.settings.CELERY_BEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'

        self.settings.CELERY_ACCEPT_CONTENT = ['json']
        self.settings.CELERY_TASK_SERIALIZER = 'json'
        self.settings.CELERY_RESULT_SERIALIZER = 'json'


    def on_startup(self):

        import djcelery
        djcelery.setup_loader()


