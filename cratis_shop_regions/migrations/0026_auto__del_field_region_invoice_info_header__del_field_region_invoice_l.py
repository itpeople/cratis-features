# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Region.invoice_info_header'
        db.delete_column(u'cratis_shop_regions_region', 'invoice_info_header')

        # Deleting field 'Region.invoice_logo'
        db.delete_column(u'cratis_shop_regions_region', 'invoice_logo')

        # Deleting field 'Region.invoice_shipping_return_address'
        db.delete_column(u'cratis_shop_regions_region', 'invoice_shipping_return_address')

        # Deleting field 'Region.tax_rate'
        db.delete_column(u'cratis_shop_regions_region', 'tax_rate')

        # Deleting field 'Region.invoice_footer_col_2'
        db.delete_column(u'cratis_shop_regions_region', 'invoice_footer_col_2')

        # Deleting field 'Region.invoice_email'
        db.delete_column(u'cratis_shop_regions_region', 'invoice_email')

        # Deleting field 'Region.invoice_footer_col_3'
        db.delete_column(u'cratis_shop_regions_region', 'invoice_footer_col_3')

        # Deleting field 'Region.invoice_company_name'
        db.delete_column(u'cratis_shop_regions_region', 'invoice_company_name')

        # Deleting field 'Region.invoice_footer_col_1'
        db.delete_column(u'cratis_shop_regions_region', 'invoice_footer_col_1')

        # Deleting field 'Region.admin_email'
        db.delete_column(u'cratis_shop_regions_region', 'admin_email')

        # Removing M2M table for field priceLists on 'Region'
        db.delete_table(db.shorten_name(u'cratis_shop_regions_region_priceLists'))

        # Removing M2M table for field countries on 'Region'
        db.delete_table(db.shorten_name(u'cratis_shop_regions_region_countries'))

        # Removing M2M table for field languages on 'Region'
        db.delete_table(db.shorten_name(u'cratis_shop_regions_region_languages'))


    def backwards(self, orm):
        # Adding field 'Region.invoice_info_header'
        db.add_column(u'cratis_shop_regions_region', 'invoice_info_header',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Region.invoice_logo'
        db.add_column(u'cratis_shop_regions_region', 'invoice_logo',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Region.invoice_shipping_return_address'
        db.add_column(u'cratis_shop_regions_region', 'invoice_shipping_return_address',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Region.tax_rate'
        db.add_column(u'cratis_shop_regions_region', 'tax_rate',
                      self.gf('django.db.models.fields.DecimalField')(default=False, max_digits=7, decimal_places=2),
                      keep_default=False)

        # Adding field 'Region.invoice_footer_col_2'
        db.add_column(u'cratis_shop_regions_region', 'invoice_footer_col_2',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Region.invoice_email'
        db.add_column(u'cratis_shop_regions_region', 'invoice_email',
                      self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Region.invoice_footer_col_3'
        db.add_column(u'cratis_shop_regions_region', 'invoice_footer_col_3',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Region.invoice_company_name'
        db.add_column(u'cratis_shop_regions_region', 'invoice_company_name',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Region.invoice_footer_col_1'
        db.add_column(u'cratis_shop_regions_region', 'invoice_footer_col_1',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Region.admin_email'
        db.add_column(u'cratis_shop_regions_region', 'admin_email',
                      self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True),
                      keep_default=False)

        # Adding M2M table for field priceLists on 'Region'
        m2m_table_name = db.shorten_name(u'cratis_shop_regions_region_priceLists')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('region', models.ForeignKey(orm[u'cratis_shop_regions.region'], null=False)),
            ('pricelist', models.ForeignKey(orm[u'cratis_shop_regions.pricelist'], null=False))
        ))
        db.create_unique(m2m_table_name, ['region_id', 'pricelist_id'])

        # Adding M2M table for field countries on 'Region'
        m2m_table_name = db.shorten_name(u'cratis_shop_regions_region_countries')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('region', models.ForeignKey(orm[u'cratis_shop_regions.region'], null=False)),
            ('country', models.ForeignKey(orm[u'cratis_shop_regions.country'], null=False))
        ))
        db.create_unique(m2m_table_name, ['region_id', 'country_id'])

        # Adding M2M table for field languages on 'Region'
        m2m_table_name = db.shorten_name(u'cratis_shop_regions_region_languages')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('region', models.ForeignKey(orm[u'cratis_shop_regions.region'], null=False)),
            ('language', models.ForeignKey(orm[u'cratis_shop_regions.language'], null=False))
        ))
        db.create_unique(m2m_table_name, ['region_id', 'language_id'])


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'cratis_rules.rule': {
            'Meta': {'object_name': 'Rule'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'rule': ('django.db.models.fields.TextField', [], {}),
            'rule_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cratis_rules.RuleType']", 'null': 'True'})
        },
        u'cratis_rules.ruletype': {
            'Meta': {'object_name': 'RuleType'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        u'cratis_shop_payment.paymentmethod': {
            'Meta': {'ordering': "('sorting',)", 'object_name': 'PaymentMethod'},
            'config': ('cratis_shop_payment.fields.YamlFancyField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'payment_method_logo'", 'null': 'True', 'to': "orm['filer.Image']"}),
            'method': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            'picture': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'payment_method_picture'", 'null': 'True', 'to': "orm['filer.Image']"}),
            'slug': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'sorting': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'cratis_shop_regions.country': {
            'Meta': {'object_name': 'Country'},
            'country': ('django_countries.fields.CountryField', [], {'default': "''", 'max_length': '2'}),
            'delivery_rule_row': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'country_order_row_rule'", 'null': 'True', 'to': u"orm['cratis_rules.Rule']"}),
            'delivery_rule_total': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'country_order_total_rule'", 'null': 'True', 'to': u"orm['cratis_rules.Rule']"}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'express_delivery_index': ('django.db.models.fields.FloatField', [], {}),
            'free_delivery_at': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'cratis_shop_regions.countrydeliveryweightindex': {
            'Meta': {'object_name': 'CountryDeliveryWeightIndex'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'delivery_weights'", 'to': u"orm['cratis_shop_regions.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.FloatField', [], {}),
            'weight_from': ('django.db.models.fields.FloatField', [], {}),
            'weight_to': ('django.db.models.fields.FloatField', [], {})
        },
        u'cratis_shop_regions.countryextrafield': {
            'Meta': {'object_name': 'CountryExtraField'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'extra_fields'", 'to': u"orm['cratis_shop_regions.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'cratis_shop_regions.currency': {
            'Meta': {'object_name': 'Currency'},
            'currency': ('cratis_shop_regions.models.CurrencyField', [], {'default': "''", 'max_length': '10'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_on_site': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'}),
            'symbol_on_site': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'})
        },
        u'cratis_shop_regions.currencypayemntmethod': {
            'Meta': {'unique_together': "(('region', 'currency'),)", 'object_name': 'CurrencyPayemntMethod'},
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cratis_shop_regions.Currency']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'payment_methods': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cratis_shop_payment.PaymentMethod']", 'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'payments'", 'to': u"orm['cratis_shop_regions.Region']"})
        },
        u'cratis_shop_regions.language': {
            'Meta': {'object_name': 'Language'},
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'}),
            'locale_code': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'}),
            'name_on_site': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'})
        },
        u'cratis_shop_regions.pricelist': {
            'Meta': {'object_name': 'PriceList'},
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_main': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'price_list': ('cratis_shop_regions.models.PriceListField', [], {'default': "''", 'max_length': '255'})
        },
        u'cratis_shop_regions.region': {
            'Meta': {'object_name': 'Region'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'})
        },
        u'cratis_shop_regions.regionextrafield': {
            'Meta': {'object_name': 'RegionExtraField'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'extra_fields'", 'to': u"orm['cratis_shop_regions.Region']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        'filer.file': {
            'Meta': {'object_name': 'File'},
            '_file_size': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'folder': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'all_files'", 'null': 'True', 'to': "orm['filer.Folder']"}),
            'has_all_mandatory_data': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'original_filename': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'owned_files'", 'null': 'True', 'to': u"orm['ticcet.User']"}),
            'polymorphic_ctype': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polymorphic_filer.file_set+'", 'null': 'True', 'to': u"orm['contenttypes.ContentType']"}),
            'sha1': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40', 'blank': 'True'}),
            'uploaded_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'filer.folder': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('parent', 'name'),)", 'object_name': 'Folder'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'filer_owned_folders'", 'null': 'True', 'to': u"orm['ticcet.User']"}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['filer.Folder']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'uploaded_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'filer.image': {
            'Meta': {'object_name': 'Image', '_ormbases': ['filer.File']},
            '_height': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            '_width': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'author': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'date_taken': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'default_alt_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'default_caption': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'file_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['filer.File']", 'unique': 'True', 'primary_key': 'True'}),
            'must_always_publish_author_credit': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'must_always_publish_copyright': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'subject_location': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '64', 'null': 'True', 'blank': 'True'})
        },
        u'ticcet.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'money_spent': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '8', 'decimal_places': '3', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'subscribe': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        }
    }

    complete_apps = ['cratis_shop_regions']