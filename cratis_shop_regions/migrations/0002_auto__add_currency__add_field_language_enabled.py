# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Currency'
        db.create_table(u'cratis_shop_regions_currency', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('currency', self.gf('cratis_shop_regions.models.CurrencyField')(default='', max_length=10)),
            ('name_on_site', self.gf('django.db.models.fields.CharField')(default='', max_length=250)),
            ('enabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'cratis_shop_regions', ['Currency'])

        # Adding field 'Language.enabled'
        db.add_column(u'cratis_shop_regions_language', 'enabled',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding M2M table for field currencies on 'Region'
        m2m_table_name = db.shorten_name(u'cratis_shop_regions_region_currencies')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('region', models.ForeignKey(orm[u'cratis_shop_regions.region'], null=False)),
            ('currency', models.ForeignKey(orm[u'cratis_shop_regions.currency'], null=False))
        ))
        db.create_unique(m2m_table_name, ['region_id', 'currency_id'])


    def backwards(self, orm):
        # Deleting model 'Currency'
        db.delete_table(u'cratis_shop_regions_currency')

        # Deleting field 'Language.enabled'
        db.delete_column(u'cratis_shop_regions_language', 'enabled')

        # Removing M2M table for field currencies on 'Region'
        db.delete_table(db.shorten_name(u'cratis_shop_regions_region_currencies'))


    models = {
        u'cratis_shop_regions.currency': {
            'Meta': {'object_name': 'Currency'},
            'currency': ('cratis_shop_regions.models.CurrencyField', [], {'default': "''", 'max_length': '10'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_on_site': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'})
        },
        u'cratis_shop_regions.language': {
            'Meta': {'object_name': 'Language'},
            'code': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'locale_code': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'name_on_site': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'})
        },
        u'cratis_shop_regions.pricelist': {
            'Meta': {'object_name': 'PriceList'},
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price_list': ('cratis_shop_regions.models.PriceListField', [], {'default': "''", 'max_length': '10'})
        },
        u'cratis_shop_regions.region': {
            'Meta': {'object_name': 'Region'},
            'currencies': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cratis_shop_regions.Currency']", 'null': 'True', 'blank': 'True'}),
            'geoip_filter_query': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'languages': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cratis_shop_regions.Language']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'}),
            'priceLists': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cratis_shop_regions.PriceList']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['cratis_shop_regions']