# -*- coding: utf-8 -*-
from cratis_rules.models import Rule
from cratis_shop_payment.models import PaymentMethod

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField
from django.conf import settings


ALL_LANGUAGES = (
        ('eng', 'English'),
        ('spa', 'Spanish'),
        ('ger', 'German'),
        ('swe', 'Swedish'),
        ('fin', 'Finnish'),
        ('rus', 'Russian'),
        ('est', 'Estonian'),
        ('lat', 'Latvian'),
        ('lit', 'Lithuanian'),
        ('gre', 'Greek'),
    )


class CurrencyField(models.CharField):
    pass
    # frontend = inject.attr(CratisShopFrontend)
    #
    # def __init__(self, *args, **kwargs):
    #
    #     choices = []
    #     for item in self.frontend.get_currency_list().values():
    #         choices.append((item['code'], item['name']))
    #
    #     kwargs['choices'] = choices
    #
    #     super(CurrencyField, self).__init__(*args, **kwargs)


class PriceListField(models.CharField):
    pass
    # frontend = inject.attr(CratisShopFrontend)

    # def __init__(self, *args, **kwargs):
    #
    #     choices = []
    #     for item in self.frontend.get_price_lists().values():
    #         choices.append((item['name'], item['name']))
    #
    #     kwargs['choices'] = choices
    #
    #     super(PriceListField, self).__init__(*args, **kwargs)

from south.modelsinspector import add_introspection_rules
add_introspection_rules([], ["^cratis_shop_regions"])


class PriceList(models.Model):
    price_list = PriceListField(max_length=255, null=False, default="", blank=False)
    is_main = models.BooleanField(default=False)
    enabled = models.BooleanField(default=False)

    def __unicode__(self):
        return self.price_list


class Currency(models.Model):
    currency = CurrencyField(max_length=10, null=False, default="", blank=False)
    name_on_site = models.CharField(max_length=250, null=False, default="", blank=False)
    symbol_on_site = models.CharField(max_length=10, null=False, default="", blank=False)
    enabled = models.BooleanField(default=False)

    def __unicode__(self):
        return '%s (%s)' % (self.name_on_site, self.currency)


class Language(models.Model):
    language = models.CharField(max_length=10, null=False, default="", blank=False, choices=ALL_LANGUAGES)
    name_on_site = models.CharField(max_length=250, null=False, default="", blank=False)
    locale_code = models.CharField(max_length=10, null=False, default="", blank=False, choices=settings.LANGUAGES)
    enabled = models.BooleanField(default=False)

    def __unicode__(self):
        return '%s (%s)' % (self.name_on_site, self.language)


class Country(models.Model):
    country = CountryField(null=False, blank=False, default="")
    enabled = models.BooleanField(default=False)
    express_delivery_index = models.FloatField()
    free_delivery_at = models.FloatField(null=True, blank=True)

    delivery_rule_row = models.ForeignKey(Rule, null=True, blank=True, related_name='country_order_row_rule')
    delivery_rule_total = models.ForeignKey(Rule, null=True, blank=True, related_name='country_order_total_rule')

    def get_row_rule(self):
        if self.delivery_rule_row:
            return self.delivery_rule_row.get_js()
        else:
            return None

    def get_total_rule(self):
        if self.delivery_rule_total:
            return self.delivery_rule_total.get_js()
        else:
            return None


    def __unicode__(self):
        return '%s' % (unicode(self.country.name),)


class Region(models.Model):
    name = models.CharField(max_length=250, null=False, default="", blank=False)

    # languages = models.ManyToManyField('Language', null=True, blank=True, verbose_name=_('Languages'))
    # priceLists = models.ManyToManyField('PriceList', null=True, blank=True, verbose_name=_('Price lists'))
    # countries = models.ManyToManyField('Country', null=True, blank=True, verbose_name=_('Countries'))

    # tax_rate = models.DecimalField(default=False, decimal_places=2, max_digits=7)

    # root_page = PageField(null=True, blank=True, verbose_name=_('Root cms page'))

    # admin_email = models.EmailField(blank=True, null=True)
    #
    # invoice_email = models.EmailField(blank=True, null=True)
    # invoice_company_name = models.CharField(max_length=200, blank=True, null=True)
    # invoice_info_header = models.TextField(blank=True, null=True)
    # invoice_shipping_return_address = models.TextField(blank=True, null=True)
    # invoice_footer_col_1 = models.TextField(blank=True, null=True)
    # invoice_footer_col_2 = models.TextField(blank=True, null=True)
    # invoice_footer_col_3 = models.TextField(blank=True, null=True)
    #
    # invoice_logo = models.ImageField(null=True, blank=True, upload_to='static/logos')

    @property
    def currencies(self):
        return [x.currency for x in self.payments.all() if x.currency.enabled]

    def __unicode__(self):
        return self.name


class CurrencyPayemntMethod(models.Model):
    region = models.ForeignKey('Region', null=False, blank=False, verbose_name='Currency & method', related_name='payments')
    currency = models.ForeignKey('Currency', null=False, blank=False, verbose_name='Currency')
    payment_methods = models.ManyToManyField(PaymentMethod, null=True, blank=True, verbose_name=_('Payment methods'))

    class Meta:
        unique_together = (("region", "currency"),)



class CountryDeliveryWeightIndex(models.Model):
    weight_from = models.FloatField()
    weight_to = models.FloatField()
    value = models.FloatField()
    country = models.ForeignKey(Country, related_name='delivery_weights')


class CountryExtraField(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    value = models.CharField(max_length=255, null=False, blank=True)
    country = models.ForeignKey(Country, related_name='extra_fields')


class RegionExtraField(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    value = models.CharField(max_length=255, null=False, blank=True)
    region = models.ForeignKey(Region, related_name='extra_fields')




