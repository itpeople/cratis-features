from chosen.widgets import ChosenSelectMultiple
from cratis_shop_regions.models import CountryExtraField, RegionExtraField, PriceList, Language, Currency, Country, \
    Region, CountryDeliveryWeightIndex, CurrencyPayemntMethod
from django.contrib import admin
from django.db import models

class InlineCountryDeliveryIndex(admin.TabularInline):

    model = CountryDeliveryWeightIndex
    extra = 20

class InlineCountryExtraField(admin.TabularInline):

    model = CountryExtraField
    extra = 3



class InlineRegionCurrencyPayemntMethod(admin.TabularInline):

    model = CurrencyPayemntMethod
    extra = 3


class CountryAdmin(admin.ModelAdmin):
    inlines = [InlineCountryDeliveryIndex]
    save_as = True


class RegionAdmin(admin.ModelAdmin):
    inlines = [InlineRegionCurrencyPayemntMethod]
    class Media:
        css = {
            "all": ('bower/chosen_v1.1.0/chosen.min.css',)
        }
        js = (
            "bower/chosen_v1.1.0/chosen.jquery.min.js",
            "js/admin.js",
        )
admin.site.register(PriceList)
admin.site.register(Language)
admin.site.register(Currency)
admin.site.register(Country, CountryAdmin)
admin.site.register(Region, RegionAdmin)



