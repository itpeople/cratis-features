from django.conf.urls import patterns
from cratis.features import Feature


class ShoppingCartRegions(Feature):
    def configure_settings(self):
        super(ShoppingCartRegions, self).configure_settings()

        self.append_apps([
            "cratis_shop_regions"
        ])

        # self.append_template_processor([
        #     'cratis_shop_regions.context.region_context'
        # ])