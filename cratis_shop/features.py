from cratis.features import Feature

from cratis_shop.shop.api import ShopBackend, ShopFrontend
from django.conf.urls import patterns, include
from elasticsearch import Elasticsearch


def import_class_by_name(cl):
    d = cl.rfind(".")
    classname = cl[d+1:len(cl)]
    m = __import__(cl[0:d], globals(), locals(), [classname])

    return getattr(m, classname)


class CratisShop(Feature):

    backend_class = None

    def __init__(self, shop_backend, image_sizes):
        super(CratisShop, self).__init__()

        self.backend_class = shop_backend
        self.image_sizes = image_sizes

    def configure_settings(self):
        self.append_template_processor(['cratis_shop.context.global_vars'])

        self.append_apps(['cratis_shop'])

    def configure_urls(self, urls):
        urls += patterns('',
                         (r'^', include('cratis_shop.urls')),
                         )

    def configure_services(self, binder):
        from cratis_shop.shop.frontend import CratisShopFrontend

        binder.bind(ShopBackend, import_class_by_name(self.backend_class)())
        shop_frontend = CratisShopFrontend()
        shop_frontend.image_sizes = self.image_sizes

        binder.bind(ShopFrontend, shop_frontend)

        binder.bind(Elasticsearch, Elasticsearch(hosts=('elasticsearch',)))

