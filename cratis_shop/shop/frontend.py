from contextlib import contextmanager
import json
import math

from django.core.cache import cache
from django.core.urlresolvers import reverse
from elasticsearch.exceptions import NotFoundError, TransportError
from cratis_shop.shop.api import ShopFrontend, ShopBackend
import elasticsearch


try:
    from ficloudutils import collect_servers
except ImportError:
    def collect_servers(service, port):
        return None


@contextmanager
def index_swap(es, index_name):
    a_index = '%s_a' % index_name
    b_index = '%s_b' % index_name

    if es.indices.exists_alias(index_name) and es.indices.get_alias(index_name).keys()[0] == b_index:
        new_index = a_index
        old_index = b_index
    else:
        new_index = b_index
        old_index = a_index

    if es.indices.exists(new_index):
        es.indices.delete(new_index)

    try:
        yield new_index

        es.indices.put_alias(index_name, new_index)

        if es.indices.exists(old_index):
            es.indices.delete(old_index)

    except Exception as e:

        if es.indices.exists(new_index):
            es.indices.delete(new_index)

        raise e


class CratisShopFrontend(ShopFrontend):

    # es = inject.attr(elasticsearch.Elasticsearch)

    def __init__(self):
        super(CratisShopFrontend, self). __init__()

        self.caetgory_param_list = {}
        self.image_sizes = {
            'products': {},
            'categories': {},
            'sub_categories': {}
        }

    def get_backend(self):
        """
        :rtype: cratis_shop.api.ShopBackend
        """

        return self._backend

    def reindex_products(self):

        with index_swap(self.es, 'shop') as new_index:

            # self.es.put_settings(
            #     index="shop",
            #     body={

            #         "settings": {
            #             "analysis": {
            #                 "analyzer": {
            #                     "ngram_analyzer": {
            #                         "tokenizer": "my_ngram_tokenizer"
            #                     }
            #                 },
            #                 "tokenizer": {
            #                     "ngram_tokenizer": {
            #                         "type": "nGram",
            #                         "min_gram": "2",
            #                         "max_gram": "15",
            #                         "token_chars": ["letter", "digit"]
            #                     }
            #                 }
            #             }
            #         }
            #     }
            # )
            #
            # self.es.put_mapping(
            #     index="shop",
            #     body={
            #         "product": {
            #             "properties": {
            #                 "name": {
            #                     "boost": 10,
            #                     "type": "string",
            #                     "analyzer": "ngram_analyzer"
            #                 }
            #             }
            #         }
            #     }
            # )

            params = {}
            all_params = self.get_backend().fetch_parameters()

            for p in all_params:
                p['is_technical'] = p['parameterGroupName'].startswith('grandex_technical_')
                p['is_filter'] = p['parameterGroupName'].startswith('grandex_filter_')

                self.es.index(
                    index=new_index,
                    doc_type="param_data",
                    id=p['parameterID'],
                    body=p
                )
                params[p['parameterID']] = p



            parentGroups = {}

            print('Inexing categories ...')
            print('-' * 40)
            def index_category(category, parent=0):

                if 'showInWebshop' in category and int(category['showInWebshop']) == 0:
                    return

                print(category['name'].encode('utf-8'))
                category['parent'] = parent
                self.es.index(
                    index=new_index,
                    doc_type="cats",
                    id=category['productGroupID'],
                    body=category
                )

                if category['subGroups']:
                    category['subGroups'] = [x for x in category['subGroups'] if not 'showInWebshop' in category or int(x['showInWebshop']) == 1]

                    for subgroup in category['subGroups']:
                        index_category(subgroup, category['productGroupID'])
                        parentGroups[subgroup['productGroupID']] = category['productGroupID']

            for category in self.get_backend().fetch_categories():
                index_category(category)

            print('Indexing products ...')
            print('-' * 40)
            for product in self.get_backend().fetch_products():

                if isinstance(product, dict):
                    product_ = product
                else:
                    product_ = product.raw_data


                print(product_['name'].encode('utf-8'))

                pparams = product_['parameters']
                pdict = {}

                for p in pparams:
                    pdict[int(p['parameterID'])] = p

                    if params:
                        p['param'] = params[int(p['parameterID'])]

                        if p['param']['is_filter'] and p['parameterType'] == 'NUMERIC':
                            filter_key = 'filter_%d' % int(p['parameterID'])
                            product_[filter_key] = p['parameterValue']


                product_['parameters'] = pdict

                self.es.index(
                    index=new_index,
                    doc_type="product",
                    id=product_['productID'],
                    body=product_
                )

            print('Inexing category params ...')
            for groupId, data in self.caetgory_param_list.items():
                self.es.index(
                    index=new_index,
                    doc_type="cat_params",
                    id=groupId,
                    body=data
                )

            print('Inexing product variations ...')
            for p in self.get_backend().fetch_product_variations():
                self.es.index(
                    index=new_index,
                    doc_type="variations",
                    id=p['dimensionID'],
                    body=p
                )

            print('Inexing price lists ...')
            for p in self.get_backend().fetch_price_lists():
                self.es.index(
                    index=new_index,
                    doc_type="priceLists",
                    id=p['pricelistID'],
                    body=p
                )

            print('Inexing curency lists ...')
            for p in self.get_backend().fetch_currency_list():
                self.es.index(
                    index=new_index,
                    doc_type="currency",
                    id=p['currencyID'],
                    body=p
                )




    def get_category_params(self, cat_id):
        try:
            result = self.es.get(index="shop", doc_type="cat_params", id=int(cat_id))['_source']
            return result
        except NotFoundError:
            return {}

    def get_params_list(self):
        try:
            result = self.es.search(index='shop', doc_type="param_data", body={'size': 1000})

            params = dict([
                (int(row['_source']['parameterID']), row['_source'])
                for row in result['hits']['hits']
            ])

            return params
        except TransportError:
            return []


    def get_categories(self):
        try:
            result = self.es.search(index='shop', doc_type="cats", body={"query": {
                'match': {'parent': 0}
            }})
        except TransportError:
            return []

        return [row['_source'] for row in result['hits']['hits']]


    def get_price_lists(self):
        try:
            result = self.es.search(index='shop', doc_type="priceLists")
            return dict([(row['_source']['name'], row['_source']) for row in result['hits']['hits']])
        except TransportError:
            return {}

    def get_price_list(self, _id):
        return self.es.get(index='shop', doc_type='priceLists', id=_id)['_source']

    def get_currency_list(self):
        try:
            result = self.es.search(index='shop', doc_type="currency")
            return dict([(row['_source']['code'], row['_source']) for row in result['hits']['hits']])
        except TransportError:
            return {}

    def get_currency(self, _id):
        return self.es.get(index='shop', doc_type='currency', code=_id)['_source']


    def get_parameter(self, param_id=0):
        try:
            return self.es.get(index="shop", doc_type="param_data", id=int(param_id))['_source']
        except NotFoundError:
            return None

    def get_facet_declarations(self, category=None):

        factes = {}

        all_params = self.get_params_list()

        if category:
            for param_id, param in all_params.items():

                if param['is_filter']:

                    name = 'filter_%d' % param_id

                    def format_key(key):
                        return '%s %s' % (key, param['parameterUnitName'])

                    factes[name] = {
                        'name': param['parameterName'],
                        'type': 'terms',
                        'field': name,
                        'key_formater': format_key
                    }

        factes['price'] = {
            'name': 'Price',
            'type': 'range',
            'field': 'price',
            'interval': 10,
        }
        return factes

    def format_results(self, facets_, page, page_size, result, category):

        # pagination
        total = result['hits']['total']
        total_pages = int(math.ceil(total / float(page_size))) + 1
        if total_pages > 10:
            pages = range(max(page - 3, 1), min(page + 3, total_pages - 1))
            pages.append(total_pages - 1)
        else:
            pages = range(1, total_pages)

        cat_ = self.es.get(index='shop', doc_type='cats', id=category)['_source'] if category else None

        if cat_:
            cat_['url'] = reverse('shop_category', kwargs={'id': category})

            if int(cat_['parent']) > 0:
                cat_['parentGroup'] = self.es.get(index='shop', doc_type='cats', id=int(cat_['parent']))['_source']
                cat_['parentGroup']['url'] = reverse('shop_category', kwargs={'id': cat_['parent']})
        #
        return {
            'total': total,
            'category': cat_,
            'page': page,
            'prev_page': max(1, page - 1),
            'next_page': min(page + 1, total_pages - 1),
            'pages': pages,
            'facets': facets_,
            'rows': [row['_source'] for row in result['hits']['hits']]
        }

    def apply_sorting(self, body, sort):
        if sort:
            def sort_list(x):
                return {
                    'lowest': {"price": {"order": "asc"}},
                    'highest': {"price": {"order": "desc"}},
                    'name': {"name": {"order": "asc"}},
                }[x]

            body['sort'] = sort_list(sort)

    def apply_query(self, body, brand, category, facet_selections, product):
        multi_match = []

        # multi_match.append({"match": {"parentProductID": None}})

        if category:
            multi_match.append({"match": {"groupID": category}})

        if 'q' in facet_selections and len(str(facet_selections['q'])):
            multi_match.append({
                "multi_match": {
                    "query": facet_selections['q'],
                    "fields": ["name", "description",
                               'longdescENG', 'longdescFIN', 'longdescRUS',
                               'descriptionENG', 'descriptionFIN', 'descriptionRUS']
                }
            })

        else:
            multi_match.append({"match": {"isVariation": False}})

        if product:
            multi_match.append({"match": {"productID": product}})

        # if brand:
        #    multi_match.append({"match": {"brandID": brand}})

        if len(multi_match):
            body['query'] = {"bool": {
                'must': multi_match
            }}
        return multi_match

    def apply_facet_filter(self, body, facet_declarations, facet_selections, price):
        filter_result = []
        for name, declaration in facet_declarations.items():

            if declaration['type'] == 'range':
                if price:
                    try:
                        ffilter = {
                            'range': {
                                declaration['field']: price
                            }
                        }
                        filter_result.append(dict(ffilter))
                    except:
                        pass

            if declaration['type'] == 'terms':
                try:
                    value = facet_selections[name]
                    ffilter = {
                        'term': {
                            declaration['field']: value
                        }
                    }
                    filter_result.append(dict(ffilter))
                except:
                    pass

                    # if facet_selections.has_key('select'):
                    #    try:
                    #        value = facet_selections['select']
                    #        ffilter = {
                    #            'term': {
                    #                'parameterID': value
                    #            }
                    #        }
                    #        filter_result.append(dict(ffilter))
                    #    except:
                    #        pass
        if filter_result:
            filter_result = {"and": filter_result}
        else:
            filter_result = {}
        body['filter'] = filter_result

    def apply_facet_query(self, body, facet_declarations):
        fquery = {}
        for name, declaration in facet_declarations.items():
            if declaration['type'] == 'terms':
                fquery[name] = {
                    "terms": {"field": declaration['field']}
                }

            if declaration['type'] == 'range' and 'interval' in declaration:
                fquery[name] = {
                    "histogram": {"field": declaration['field'], "interval": declaration['interval']}
                }

        body['facets'] = fquery

    def format_facets(self, facet_declarations, facet_selections, result):
        # facet results
        facets_ = result['facets']
        for name in facet_declarations:
            declaration = facet_declarations[name]
            facets_[name]['name'] = declaration['name']

            if declaration['type'] == 'terms':
                for entry in facets_[name]['terms']:
                    entry['value'] = declaration['key_formater'](entry['term'])
                    if name in facet_selections and facet_selections[name] == unicode(entry['term']):
                        entry['selected'] = True
                    else:
                        entry['selected'] = False
        return facets_

    def get_product(self, product_id):

        return self.es.get(index='shop', doc_type='product', id=product_id)['_source']

    def get_product_by_code(self, code):

        return self.es.get(index='shop', doc_type='product', code=code)['_source']

    def get_category(self, category_id):
        return self.es.get(index='shop', doc_type='cats', id=category_id)['_source']

    def search(
            self, facet_selections=None, page=1, page_size=10, category=None, product=None, brand=None, sort=None,
            price=None, fast=None):

        body = {
            "from": page_size * (page - 1),
            "size": page_size
        }

        facet_selections = facet_selections or {}

        facet_declarations = self.get_facet_declarations(category=category)

        self.apply_facet_query(body, facet_declarations)

        self.apply_facet_filter(body, facet_declarations, facet_selections, price)

        self.apply_query(body, brand, category, facet_selections, product)

        self.apply_sorting(body, sort)

        result = self.es.search(index='shop', body=body)

        if fast:
            return {'rows': [row['_source'] for row in result['hits']['hits']]}

        facets_ = self.format_facets(facet_declarations, facet_selections, result)

        return self.format_results(facets_, page, page_size, result, category)
