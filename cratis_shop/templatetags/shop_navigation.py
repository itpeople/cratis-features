from django.utils.translation import get_language
from django import template

from cratis_shop.shop.frontend import CratisShopFrontend
from cratis_erply.api import get_client
from cratis_shop.views import apply_group_translations
from cratis_shop_regions.models import Language
from elasticsearch import TransportError


register = template.Library()


# @register.simple_tag(takes_context=True)
# def erply_locale(context, obj, field):
#     request = context['request']
#     return obj['{0}{1}'.format(field, lang_chars(request.session['django_language']))]


@register.inclusion_tag("nav/categories.html", takes_context=True)
def category_header(context):
    request = context['request']
    colors = ['lime', 'green', 'blue', 'dark_blue', 'purple']
    return {'categories': _categories(), 'request': request, 'colors': colors}


def _categories():
    forntend = CratisShopFrontend()
    categories = forntend.get_categories()


    # get counts on groupID
    body = {"facets":
           {'groups': {
                    'terms': {
                        'field': 'groupID',
                        "size": 10000  # no limit
                    }
           }}
    }

    try:
        products = forntend.es.search(index='shop', body=body)
    except TransportError:
        return []

    # set count
    counts = {}
    for value in products['facets']['groups']['terms']:
        try:
            counts[value['term']] = value['count']
        except:
            counts[value['term']] = 0

    try:
        current_lang = Language.objects.get(locale_code=get_language())


    except Language.DoesNotExist:
        return

    # set count in category
    for category in categories:

        apply_group_translations(category, lang=current_lang)

        if int(category['productGroupID']) in counts:
            category['count'] = counts[int(category['productGroupID'])]
        else:
            category['count'] = 0

        if category['subGroups']:
            sub = []
            for sub_category in category['subGroups']:

                apply_group_translations(sub_category, lang=current_lang)

                if int(sub_category['productGroupID']) in counts:
                    cnt = counts[int(sub_category['productGroupID'])]
                    # if cnt > 0:
                    sub_category['count'] = cnt
                else:
                    sub_category['count'] = 0

                sub.append(sub_category)

            category['subGroups'] = sub

    # print len(categories)


    # cache.set('categories1', categories, settings.TIME_CACHE_NAVBAR)
    return categories


@register.inclusion_tag("navbar.html", takes_context=True)
def navbar(context):
    request = context['request']
    return {'request': request}

@register.inclusion_tag("nav/username.html", takes_context=True)
def username(context):
    request = context['request']
    return {'request': request}


@register.inclusion_tag("nav/profile.html", takes_context=True)
def profile(context):
    request = context['request']
    address = get_client().get_address(request.user.erply_customer_id)
    count_address = 0
    for item in address:
        count_address += 1
    return {'request': request, 'count_address': count_address}
