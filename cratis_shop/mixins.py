from __future__ import unicode_literals


from django.views.generic import TemplateView
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect


# mixin for ajax
class AjaxOnlyView(TemplateView):

    def dispatch(self, request, *args, **kwargs):
        if not request.is_ajax():
            return self.http_method_not_allowed(request, *args, **kwargs)
        return super(AjaxOnlyView, self).dispatch(request, *args, **kwargs)


# login required mixin
class LoginRequiredMixin(object):

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_anonymous():
            return HttpResponseRedirect(reverse('account_login'))
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)
