from cratis_shop.shop.frontend import CratisShopFrontend
from cratis_shop.views import ShopAwareView
from django.views.generic import TemplateView


def test_shop_aware_view():
    class SomeView(TemplateView, ShopAwareView):
        template_name = 'foo'

    assert isinstance(SomeView().frontend, CratisShopFrontend)