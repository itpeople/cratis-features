from __future__ import unicode_literals, absolute_import

from django import forms
from django.utils.translation import ugettext_lazy as _


class CheckoutAddressForm(forms.Form):
    country = forms.ChoiceField(
        choices=country_choices_form, label=_('Country'))
    street = forms.CharField(required=True, label=_('Street'))
    city = forms.CharField(required=True, label=_('City'))
    post = forms.CharField(required=True,
                           label=_('Post'),
                           widget=forms.TextInput(attrs={'id': 'checkout-post'}))
    first_name = forms.CharField(required=True, label=_('First name'))
    last_name = forms.CharField(required=True, label=_('Last name'))
    email = forms.EmailField(required=True, label=_('Email'))
    phone = forms.CharField(label=_('Phone'),
                            widget=forms.TextInput(attrs={'id': 'checkout-phone'}))
