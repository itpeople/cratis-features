from base64 import b64encode
from collections import OrderedDict
import json
import string
import random
import math
import datetime
from cratis_profile.models import Address

from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.translation import get_language
from django.views.generic import TemplateView
from django.http import Http404
from django.contrib import messages
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.contrib.auth import authenticate, login
from django.views.generic.base import View
from easy_thumbnails.files import get_thumbnailer
from elasticsearch import NotFoundError

from djangular.views.mixins import JSONResponseMixin, allow_remote_invocation

from cratis_news.models import Article
from cratis_shop.shop.frontend import CratisShopFrontend
from cratis_shop_payment.models import PaymentOrder
from cratis_shop_regions.context import get_region
from cratis_shop_regions.models import Language, Region
# from cratis_erply.api import get_client
from .forms import CheckoutAddressForm
from .mixins import LoginRequiredMixin


def apply_group_translations(group, lang=None):
    current_lang = lang or Language.objects.get(locale_code=get_language())

    lang = current_lang.language

    nameField = 'name%s' % lang.upper()

    if nameField in group:
        group['name'] = group[nameField]

    if len(group['subGroups']):
        for subgroup in group['subGroups']:
            apply_group_translations(subgroup)


def apply_product_translations(product, lang=None):
    current_lang = lang or Language.objects.get(locale_code=get_language())

    lang = current_lang.language

    nameField = 'name%s' % lang.upper()
    descriptionField = 'description%s' % lang.upper()
    adescriptionField = 'description_%s' % lang
    longDescriptionField = 'longdesc%s' % lang.upper()
    alongDescriptionField = 'longdesc_%s' % lang

    attributes = {}
    if 'longAttributes' in product:
        for attr in product['longAttributes']:
            attributes[attr['attributeName']] = attr['attributeValue']

    if nameField in product:
        product['name'] = product[nameField]

    if descriptionField in product:
        product['description'] = product[descriptionField]
    elif adescriptionField in attributes:
        product['description'] = attributes[adescriptionField]

    if longDescriptionField in product:
        product['longdesc'] = product[longDescriptionField]
    elif alongDescriptionField in attributes:
        product['longdesc'] = attributes[alongDescriptionField]


def apply_pricing_policy(product, region=None):
    frontend = inject.instance(CratisShopFrontend)

    try:
        main_price = float(product['main_price'])
    except KeyError:
        main_price = None
    except TypeError:
        main_price = None

    best_price = None
    original_price = float(product['price'])

    region = region or Region.objects.all()[0]

    price_list_info = frontend.get_price_lists()

    for price_list in region.priceLists.filter(enabled=True):
        if price_list.price_list in price_list_info:
            for rule in price_list_info[price_list.price_list]['pricelistRules']:
                if rule['type'] == 'PRODUCT' and rule['id'] == product['productID']:

                    if price_list.is_main:
                        if main_price is None:
                            main_price = float(rule['price'])

                        elif main_price > float(rule['price']):
                            main_price = float(rule['price'])

                    if not best_price or best_price > float(rule['price']):
                        best_price = float(rule['price'])

    if not best_price:
        best_price = original_price

    if main_price and main_price != best_price:
        product['main_price'] = main_price
    else:
        product['main_price'] = None

    product['price'] = best_price


def apply_product_modifications(product, lang=None, region=None):
    apply_product_translations(product, lang)
    apply_pricing_policy(product, region)

    product['id'] = product['productID']

    if product['isVariation']:

        frontend = inject.instance(CratisShopFrontend)

        parent_product = frontend.get_product(product['parentProductID'])
        variation_vals = {}

        for variations in parent_product['variationList']:
            for dim in variations['dimensions']:
                variation_vals[dim['dimensionValueID']] = dim['code']

        base = reverse('shop_product', kwargs={'id': product['parentProductID']})
        product['url'] = '%s#%s' % (
            base, '-'.join(sorted([variation_vals[x['variationID']] for x in product['variationDescription']])))

        if not 'pics' in product or not product['pics']:
            product['pics'] = parent_product['pics']
    else:
        product['url'] = reverse('shop_product', kwargs={'id': product['productID']})

    for field in ('netWeight',):
        if field in product:
            try:
                product[field] = float(product[field])
            except ValueError:
                product[field] = 0.0


class ShopAwareView(object):
    frontend = inject.attr(CratisShopFrontend)
    """ @type CratisShopFrontend """


class SearchFrontendDataView(View, ShopAwareView):
    def dispatch(self, request, *args, **kwargs):

        context = {}

        try:
            size = int(self.request.GET.get('size', 50))
        except ValueError:
            size = 50

        try:
            page = int(self.request.GET.get('page', 1))
        except ValueError:
            page = 1

        context['view'] = self.request.GET.get('view', 'normal')

        sort = self.request.GET.get('sort', None)

        price_min = self.request.GET.get('priceMin', None)
        price_max = self.request.GET.get('priceMax', None)
        price = None
        if price_min and price_max:
            price = {
                'lte': int(price_max),
                'gte': int(price_min)
            }

        selections = self.request.GET

        context['size'] = size
        context['price'] = price

        category = self.request.GET.get('category')

        if category:

            cat = self.frontend.get_category(category)

            apply_group_translations(cat)

            context['category'] = cat

            if cat['parent']:
                context['parent_category'] = self.frontend.get_category(cat['parent'])

            context[
                'products'] = self.frontend.search(facet_selections=selections, page=page, page_size=size,
                                                   category=category, sort=sort, price=price)
            min_value = self.frontend.search(facet_selections=selections,
                                             category=category,
                                             page_size=1,
                                             sort='lowest',
                                             fast=True)
            max_value = self.frontend.search(facet_selections=selections,
                                             category=category,
                                             page_size=1,
                                             sort='highest',
                                             fast=True)
        else:
            context['products'] = self.frontend.search(
                facet_selections=selections, page=page, sort=sort, price=price, page_size=size)
            min_value = self.frontend.search(facet_selections=selections,
                                             page_size=1,
                                             sort='lowest',
                                             fast=True)
            max_value = self.frontend.search(facet_selections=selections,
                                             page_size=1,
                                             sort='highest',
                                             fast=True)

        try:
            context['min'] = int(min_value['rows'][0]['price'])
            context['max'] = int(max_value['rows'][0]['price'])
        except:
            context['min'] = 0
            context['max'] = 100

        context['page'] = page

        language = get_language()
        current_lang = Language.objects.get(locale_code=language)

        for product in context['products']['rows']:
            apply_product_modifications(product, lang=current_lang)

        return HttpResponse(json.dumps(context), content_type="application/json")


class SearchDebugView(TemplateView, ShopAwareView):
    template_name = 'search/debug.html'

    def get_context_data(self, **kwargs):
        context = super(SearchDebugView, self).get_context_data(**kwargs)

        if 'id' in self.request.GET:
            context['data'] = get_client().fetch_many('getProducts', {
                'productID': self.request.GET['id'],
                'recordsOnPage': 10,
                'getMatrixVariations': 1,
                # 'getReplacementProducts': 1,
                # 'getStockInfo': 1,
                # 'getRelatedProducts': 1,
                # 'getRelatedFiles': 1,
                'getAllLanguages': 1,
                'displayedInWebshop': 1,
                'getParameters': 1,
                'type': 'MATRIX,PRODUCT',
                'includeMatrixVariations': 1,
            })

        else:
            context['data'] = get_client().fetch_many('getProducts', {
                'recordsOnPage': 10,
                'getMatrixVariations': 1,
                # 'getReplacementProducts': 1,
                # 'getStockInfo': 1,
                # 'getRelatedProducts': 1,
                # 'getRelatedFiles': 1,
                'getAllLanguages': 1,
                'displayedInWebshop': 1,
                'getParameters': 1,
                'type': 'MATRIX,PRODUCT',
                'includeMatrixVariations': 1,
            })

        context['data'] = json.dumps(context['data'], sort_keys=True, indent=4)

        return context


class SearchFrontendView(TemplateView, ShopAwareView):
    template_name = 'search/index.html'

    def get_context_data(self, **kwargs):
        context = super(SearchFrontendView, self).get_context_data(**kwargs)

        data = {
            'url': reverse('search_data'),
            'category': None,
        }

        if 'id' in kwargs:
            try:
                data['category'] = self.frontend.get_category(kwargs['id'])
                apply_group_translations(data['category'])
            except NotFoundError:
                raise Http404

        if 'q' in self.request.GET:
            data['query'] = self.request.GET['q']

        context['data'] = json.dumps(data)

        return context


class FrontPageView(TemplateView, ShopAwareView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(FrontPageView, self).get_context_data(**kwargs)
        # context['products'] = self.frontend.search()
        context['articles'] = Article.objects.all()[:2]
        return context


class ProductDataView(View, ShopAwareView):
    def dispatch(self, request, *args, **kwargs):
        product = self.frontend.get_product(kwargs['id'])

        apply_product_modifications(product)

        return HttpResponse(json.dumps(product), content_type="application/json")


class ProductView(TemplateView, ShopAwareView):
    template_name = 'products/product.html'

    def get_context_data(self, **kwargs):
        context = super(ProductView, self).get_context_data(**kwargs)
        try:
            product = self.frontend.get_product(kwargs['id'])
        except NotFoundError:
            raise Http404

        variation_products = OrderedDict()
        variation_fields = {}

        apply_product_modifications(product)

        if product['hasVariations']:

            for variations in product['variationList']:
                code_list = []
                for dim in variations['dimensions']:
                    if not dim['dimensionID'] in variation_fields:
                        variation_fields[dim['dimensionID']] = {
                            'name': dim['name'],
                            'values': {}
                        }

                    variation_fields[dim['dimensionID']]['values'][dim['code']] = dim['value']
                    code_list.append(dim['code'])

                variation_products['-'.join(sorted(code_list))] = int(variations['productID'])

                context['json'] = json.dumps({
                    'variation_fields': variation_fields,
                    'variation_products': variation_products,
                    'variation_default': variation_products.keys()[0],
                    'product': product,
                    'has_variations': True,
                })
        else:
            context['json'] = json.dumps({
                'product': product,
                'has_variations': False,
                'variation_default': '_'
            })

        context['product'] = product

        return context


class CompareProductsView(TemplateView, ShopAwareView):
    template_name = 'products/compare.html'

    def get_context_data(self, **kwargs):
        context = super(CompareProductsView, self).get_context_data(**kwargs)

        selections = self.request.GET
        list_products = []

        if self.request.GET['products'] != '':
            products = self.request.GET['products'].split(',')
            for product in products:
                item = self.frontend.search(product=product)
                list_products.append(item)

        context['products'] = list_products

        return context


class CheckoutDoneView(TemplateView, ShopAwareView):
    template_name = 'cart/checkout_done.html'

    def generator(self, size=6, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))

    def dispatch(self, request, *args, **kwargs):
        if request.is_ajax():
            if request.user.is_anonymous():
                form = request.session['new_address']
                password = self.generator()

                user = get_client().create_user(
                    email=form['email'].value(),
                    first_name=form['first_name'].value(),
                    last_name=form['last_name'].value(),
                    password=password,
                )
                address = get_client(
                ).create_address(street=form['street'].value(),
                                 owner_id=user['id'],
                                 city=form['city'].value(),
                                 post=form['post'].value(),
                                 country=form['country'].value())

                user = authenticate(email=form['email'].value(),
                                    password=password)
                login(request, user)

                # send notify registred new customer (login / password)
                c = Context({'email':
                                 form['email'].value(), 'password': password})
                text_content = render_to_string(
                    'mail/register_anonymous.txt', c)
                html_content = render_to_string(
                    'mail/register_anonymous.html', c)

                email = EmailMultiAlternatives(
                    'Register new user', text_content)
                email.attach_alternative(html_content, "text/html")
                email.to = [form['email'].value(), ]
                email.send()

            cart_id = self.request.COOKIES.get('cart', None)
            count = 0
            price = 0
            weight = 0
            list_products = []
            if cart_id:
                order = Order.objects.get(order_cookies=cart_id)
                products = Product.objects.filter(order=order)
                if products:
                    for product in products:
                        item = self.frontend.search(product=product.product_id)
                        item['rows'][0]['count_buy'] = product.count
                        list_products.append(item)
                        price += float(product.count * product.price)
                        weight += float(product.weight)

                    shipping = "{0:.2f}".format(
                        settings.PRICE_SHIPPING_KG * math.ceil(weight))

            c = Context({'products': list_products,
                         'shipping': shipping,
                         'price': price})
            text_content = render_to_string(
                'mail/confirm_payment.txt', c)
            html_content = render_to_string(
                'mail/confirm_payment.html', c)

            email = EmailMultiAlternatives(
                'Confirm payment', text_content)
            email.attach_alternative(html_content, "text/html")
            email.to = [self.request.user.email, ]
            email.send()

            ajax_context = {}
            ajax_context = {'result': 'ok'}
            return HttpResponse(json.dumps(ajax_context), content_type="application/json")
        return super(CheckoutDoneView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CheckoutDoneView, self).get_context_data(**kwargs)
        messages.add_message(self.request, messages.INFO, 'Success payment!')
        return context


def add_invoice_delivery_task(region, po, order, user):
    """
    @type region: Region
    """

    from easy_thumbnails.files import get_thumbnailer
    from easy_thumbnails.processors import background, scale_and_crop, colorspace

    size = {'size': (300, 300), 'upscale': False, 'crop': False}
    thumbnailer = get_thumbnailer(region.invoice_logo)
    thumbnailer.thumbnail_processors = [colorspace, scale_and_crop, background]

    thumbnail = thumbnailer.get_thumbnail(size)
    thumbnail.open()
    logo = b64encode(thumbnail.file.read())
    thumbnail.close()

    adr = Address.objects.get(pk=order['address'])

    address_post_index = ", " + adr.postcode if adr.postcode else ''
    delivery_address = str(user.firstname) + ' ' + str(user.lastname) + "\n" + \
                       str(adr.address) + str(adr.address2) + "\n" + \
                       str(adr.city) + str(address_post_index) + ", " + str(adr.country) + "\n" + \
                       "Email: " + user.email

    rows = []


        #             u'deliveryDescription': u'',
        #             u'name': u'R\xd6KL\xc5DA MINI',
        #             u'parameters': {},
        #             u'mainGroupName': u'R\xf6k och grill spisar',
        #             u'mainGroupID': 69,
        #               u'price': 1956, u'qty': 1, u'id': 419, u'slug': u'roklada-mini',
        #             u'deliveryPrice': 0, u'url': u'/product/419/', u'hasVariations': False, u'isVariation': False,
        #             u'main_price': None, u'active': True, u'pics': [{u'id': 816, u'sizes': {
        #             u'list': u'/media/filer_public_thumbnails/filer_public/2013/03/16/mini.jpg__120x139_q85_background-white_subsampling-2.jpg',
        #             u'details': u'/media/filer_public_thumbnails/filer_public/2013/03/16/mini.jpg__340x225_q85_background-white_subsampling-2.jpg'}}],
        #             u'type': u'PRODUCT', u'groupID': [69], u'productID': 419},

    for item_id, item in order['items'].items():
        rows.append({
            'id': item_id,
            'title': item['name'],
            'qty': item['qty'],
            'unitPrice': str(item['price']),
            'totalPrice': str(item['price'] * item['qty']),
            'deliveryPrice': 0
        })

    # mock translations
    _ = lambda x: x

    invoice = {
        'id': po.document_id,
        'date': str(datetime.datetime.now()),
        'email': user.email,
        'adminEmail': region.admin_email,
        'fromEmail': region.invoice_email,
        'companyName': region.invoice_company_name,
        'infoHeader': region.invoice_info_header,
        'shippingReturnAddress': region.invoice_shipping_return_address,
        'infoFooterCol1': region.invoice_footer_col_1,
        'infoFooterCol2': region.invoice_footer_col_2,
        'infoFooterCol3': region.invoice_footer_col_3,

        'logo': logo,
        'billAddress': delivery_address,
        'shippingAddress': delivery_address,
        'subtotal': po.no_tax,
        'tax': po.tax_rate,
        'taxTotal': po.tax,
        'total': po.sum,

        'rows': rows,

        'texts': {
            'sh_invoice_no': _("INVOICE No. "),
            'sh_invoice_from': _("From:"),
            'sh_invoice_to': _("To:"),
            'invoice_col_id': _("Product ID"),
            'invoice_col_description': _("Description"),
            'invoice_col_delivery': _("Delivery"),
            'invoice_col_quantity': _("Qty"),
            'invoice_col_total': _("Total"),
            'invoice_label': _("INVOICE"),
            'invoice_date_label': _("Date: "),
            'invoice_no_label': _("INVOICE No. "),
            'invoice_bill_to': _("Bill to:"),
            'invoice_ship_to': _("Ship to:"),
            'invoice_row_no_label': _("No."),
            'invoice_notes_label': _("Notes:"),
            'invoice_subtotal': _("Subtotal: "),
            'invoice_tax': _("Tax: "),
            'invoice_tax_total': _("Tax total: "),
            'invoice_total': _("Total: "),
            '$': order['currency']['symbol'],
        }
    }

    json_data = json.dumps(invoice, indent=4)

    po.invoice_data = b64encode(json_data)
    po.save()

    from rq import Connection, Queue
    from redis import Redis

    # Tell RQ what Redis connection to use
    redis_conn = Redis(host='redis')
    q = Queue(connection=redis_conn)  # no args implies the default queue

    job = q.enqueue('rqpdf.tasks.generate_pdf', po.invoice_data)


class CheckoutView(JSONResponseMixin, LoginRequiredMixin, TemplateView, ShopAwareView):
    template_name = 'cart/checkout.html'
    form_address = CheckoutAddressForm

    checkout_service = inject.attr('checkout_service')
    """ @type: cratis_shop.service.CheckoutService """

    def po_for_order(self, order):
        po = None
        if 'cart_id' in order:
            po = PaymentOrder.objects.get(order['cart_id'])
        if not po:
            po = PaymentOrder()
            po.user = self.request.user

        return po

    @allow_remote_invocation
    def save_cart(self, order):
        po = self.po_for_order(order)
        po.body = json.dumps(order)
        po.save()

    @allow_remote_invocation
    def load_cart(self):

        pos = PaymentOrder.objects.filter(user=self.request.user, checked_out=False).order_by('-date_created')
        if pos:
            return json.loads(pos[0].body)

        return None

    @allow_remote_invocation
    def checkout(self, order):
        po = self.po_for_order(order)

        po.body = json.dumps(order)
        po.save()

        self.request.session['order_id'] = po.id

        region = get_region(self.request)

        self.checkout_service.confirm_order(region, po)

        # add_invoice_delivery_task(region, po, order, self.request.user)


    @allow_remote_invocation
    def get_delivery_calculators(self, country=None):

        # Country.objects.get(country__code=country)

        return {'row': None, 'total': None}

    def get_context_data(self, **kwargs):
        context = super(CheckoutView, self).get_context_data(**kwargs)

        context['payments'] = get_region(self.request).payments.all()
        return context
