from cratis_shop.shop.frontend import CratisShopFrontend
from django.core.management.base import BaseCommand, CommandError

from cratis_shop.shop.api import ShopBackend, ShopFrontend



def download(backend, index='shop'):
    frontend = inject.instance(ShopFrontend)
    print index

    result = frontend.es.search(index=index, doc_type="product", body={'size': 10000})
    print result

    print '-' * 40
    print 'Products'
    print '-' * 40

    for row in result['hits']['hits']:
        product = row['_source']

        print 'Sizes: %s' % frontend.image_sizes['products']
        product['pics'] = backend.download_product_pictures(product, frontend.image_sizes['products'])

        frontend.es.index(
            index=index,
            doc_type="product",
            id=product['productID'],
            body=product
        )

    print '-' * 40
    print 'Categories'
    print '-' * 40

    result = frontend.es.search(index=index, doc_type="cats", body={'size': 10000})

    for row in result['hits']['hits']:
        cat = row['_source']

        print 'Sizes: %s' % frontend.image_sizes['category']
        cat['pics'] = backend.download_category_pictures(cat,  frontend.image_sizes['category'])

        if 'subGroups' in cat:
            for subcat in cat['subGroups']:
                print 'Sizes: %s' % frontend.image_sizes['sub_category']
                subcat['pics'] = backend.download_category_pictures(subcat,  frontend.image_sizes['sub_category'])

        frontend.es.index(
            index=index,
            doc_type="cats",
            id=cat['productGroupID'],
            body=cat
        )

class Command(BaseCommand):
    help = 'Download pictures from erply'

    _backend = inject.attr(ShopBackend)


    def handle(self, *args, **options):
        download(self._backend, 'shop')

