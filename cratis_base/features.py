from cratis.features import Feature
from django.conf.urls import patterns, url, include


def show_toolbar(request):
    return True

class Common(Feature):
    """
    This feature is used by most of the django-applications
    """

    def __init__(self, sites_framework=False):
        self.sites_framework = sites_framework


    def configure_settings(self):
        s = self.settings

        self.append_apps([
            'django.contrib.auth',
            'django.contrib.contenttypes',
            'django.contrib.sessions',
            'django.contrib.messages',
            'django.contrib.staticfiles',
            'cratis_base',
        ])

        # if self.settings.DEBUG:
        #     s.DEBUG_TOOLBAR_PATCH_SETTINGS = False
        #     s.SHOW_TOOLBAR_CALLBACK = lambda foo: True
        #
        #     self.append_apps([
        #         'debug_toolbar',
        #     ])
        #
        #     self.append_middleware([
        #         'debug_toolbar.middleware.DebugToolbarMiddleware',
        #     ])

        self.append_middleware([
            'django.contrib.sessions.middleware.SessionMiddleware',
            'django.middleware.locale.LocaleMiddleware',
            'django.middleware.common.CommonMiddleware',
            'django.middleware.csrf.CsrfViewMiddleware',
            'django.contrib.auth.middleware.AuthenticationMiddleware',
            'django.contrib.messages.middleware.MessageMiddleware',
            'django.middleware.clickjacking.XFrameOptionsMiddleware',
        ])



        s.STATIC_URL = '/static/'
        s.STATIC_ROOT = s.BASE_DIR + '/var/static'

        s.MEDIA_URL = '/media/'
        s.MEDIA_ROOT = s.BASE_DIR + '/var/media'


        if self.sites_framework:
            self.append_apps([
                'django.contrib.sites',
            ])
            s.SITE_ID = 1

    def configure_urls(self, urls):
        if self.settings.DEBUG:
            urls += patterns('',
                url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': self.settings.MEDIA_ROOT}),
                # url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': self.settings.STATIC_ROOT}),
            )

        # if self.settings.DEBUG:
        #     import debug_toolbar
        #     urls += patterns('',
        #         url(r'^__debug__/', include(debug_toolbar.urls)),
        #     )


class Debug(Feature):
    def configure_settings(self):
        s = self.settings

        if s.DEBUG:

            s.DEBUG_TOOLBAR_PATCH_SETTINGS = False

            s.DEBUG_TOOLBAR_CONFIG = {
                'SHOW_TOOLBAR_CALLBACK': 'cratis_base.features.show_toolbar'
            }

            self.append_apps([
                'debug_toolbar',
                # 'debug_panel'
            ])

            self.append_middleware([
                'debug_toolbar.middleware.DebugToolbarMiddleware'
            ])


    def configure_urls(self, urls):
        s = self.settings
        if s.DEBUG:
            import debug_toolbar
            urls += patterns('',
                url(r'^__debug__/', include(debug_toolbar.urls)),
            )


class Opbeat(Feature):

    def __init__(self, config):
        super(Opbeat, self).__init__()

        self.config = config

    def configure_settings(self):
        s = self.settings

        self.append_apps([
            'opbeat.contrib.django'
        ])

        self.append_middleware([
            'opbeat.contrib.django.middleware.OpbeatAPMMiddleware'
        ])

        s.OPBEAT = self.config