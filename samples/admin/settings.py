# coding=utf-8
from configurations import values
from cratis_common.features import Common
from cratis.settings import CratisConfig
from cratis_admin.features import AdminArea

from cratis_admin_suit.features import AdminThemeSuit
import os

class Dev(CratisConfig):
    DEBUG = True
    SECRET_KEY = values.SecretValue()

    DATABASES = values.DatabaseURLValue('sqlite://var/db.sqlite3')

    FEATURES = (
        Common(),
        AdminThemeSuit(title='Sample admin'),
        AdminArea(),
    )


class Test(Dev):
    pass


class Prod(Dev):
    DEBUG = False

    FEATURES = Dev.FEATURES + (
        # your features here
    )
