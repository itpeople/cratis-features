#/bin/bash -e

virtualenv .env
source .env/bin/activate

pip install -r requirements.txt
cratis syncdb --noinput
cratis migrate --noinput

cratis runserver 0.0.0.0:8000