# coding=utf-8
from cratis.settings import CratisConfig


class Dev(CratisConfig):
    DEBUG = True
    SECRET_KEY = 'not_a_secret'

    FEATURES = (
        # your features here
    )


class Test(Dev):
    pass


class Prod(Dev):
    DEBUG = False

    FEATURES = Dev.FEATURES + (
        # your features here
    )
