package maksa.shops.reporter.domain;

import sun.misc.BASE64Decoder;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Alex Rudakov
 */
public class Invoice {

    private Integer id;

    private String email;
    private String fromEmail;

    private String adminEmail;

    private String companyName;

    private byte[] logo;
    private String date;
    
    private Map<String, String> texts = new HashMap<String, String>();

    private String infoHeader;
    private String infoFooterCol1;
    private String infoFooterCol2;
    private String infoFooterCol3;

    private String shippingAddress;
    private String shippingReturnAddress;
    private String billAddress;

    private List<InvoiceRow> rows = new ArrayList<InvoiceRow>();

    private BigDecimal subtotal;
    private BigDecimal tax;
    private BigDecimal taxTotal;
    private BigDecimal total;

    private List<String> notes = new ArrayList<String>();

    public String t(String key) {
        return texts.get(key);
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public void setLogo(String logo) {
        try {
            this.logo = new BASE64Decoder().decodeBuffer(logo);
        } catch (IOException e) {
            this.logo = null;
        }
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBillAddress() {
        return billAddress;
    }

    public void setBillAddress(String billAddress) {
        this.billAddress = billAddress;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public List<InvoiceRow> getRows() {
        return rows;
    }

    public Invoice addRow(InvoiceRow row) {
        this.rows.add(row);
        return this;
    }

    public void setRows(List<InvoiceRow> rows) {
        this.rows = rows;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public BigDecimal getTaxTotal() {
        return taxTotal;
    }

    public void setTaxTotal(BigDecimal taxTotal) {
        this.taxTotal = taxTotal;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public List<String> getNotes() {
        return notes;
    }

    public void setNotes(List<String> notes) {
        this.notes = notes;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Invoice addNote(String note) {
        this.notes.add(note);
        return this;
    }

    public String getInfoHeader() {
        return infoHeader;
    }

    public void setInfoHeader(String infoHeader) {
        this.infoHeader = infoHeader;
    }

    public String getInfoFooterCol1() {
        return infoFooterCol1;
    }

    public void setInfoFooterCol1(String infoFooterCol1) {
        this.infoFooterCol1 = infoFooterCol1;
    }

    public String getInfoFooterCol2() {
        return infoFooterCol2;
    }

    public void setInfoFooterCol2(String infoFooterCol2) {
        this.infoFooterCol2 = infoFooterCol2;
    }

    public String getInfoFooterCol3() {
        return infoFooterCol3;
    }

    public void setInfoFooterCol3(String infoFooterCol3) {
        this.infoFooterCol3 = infoFooterCol3;
    }

    public String getShippingReturnAddress() {
        return shippingReturnAddress;
    }

    public void setShippingReturnAddress(String shippingReturnAddress) {
        this.shippingReturnAddress = shippingReturnAddress;
    }

    public Map<String, String> getTexts() {
        return texts;
    }

    public void setTexts(Map<String, String> texts) {
        this.texts = texts;
    }
}
