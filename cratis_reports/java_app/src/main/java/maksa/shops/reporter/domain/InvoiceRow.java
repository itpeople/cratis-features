package maksa.shops.reporter.domain;

import java.math.BigDecimal;

/**
 * @author Alex Rudakov
 */
public class InvoiceRow {
    private Integer id;
    private String title;
    private Integer qty;
    private BigDecimal unitPrice;
    private BigDecimal totalPrice;
    private BigDecimal deliveryPrice;

    public InvoiceRow(Integer id, String title, Integer qty, BigDecimal unitPrice, BigDecimal totalPrice) {
        this.id = id;
        this.title = title;
        this.qty = qty;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
    }

    public InvoiceRow(Integer id, String title, Integer qty, BigDecimal unitPrice, BigDecimal totalPrice, BigDecimal deliveryPrice) {
        this.id = id;
        this.title = title;
        this.qty = qty;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
        this.deliveryPrice = deliveryPrice;
    }

    public BigDecimal getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(BigDecimal deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
}
