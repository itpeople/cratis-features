from django.conf.urls import patterns, include
from cratis.features import Feature


class UserProfile(Feature):

    def configure_urls(self, urls):
        urls += patterns('',
                         ('', include('cratis_profile.urls')),
        )


class LocalDbUser(Feature):

    def configure_settings(self):
        self.append_apps(['cratis_profile'])
        self.settings.AUTH_USER_MODEL = "cratis_profile.User"

    def create_adapter(self):
        from cratis_profile.profile_adapter import LocalProfileAdapter
        return LocalProfileAdapter()


    def configure_services(self, binder):
        binder.bind_to_constructor('profile_adapter', self.create_adapter)