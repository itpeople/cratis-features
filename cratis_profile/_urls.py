
from django.conf.urls import url, patterns, include
from cratis_i18n.utils import localize_url as _


urlpatterns = patterns('',

    url(_(r'^profile/$'), 'cratis_profile.views.profile', name='profile')
)
