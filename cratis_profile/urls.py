from django.conf.urls import url, patterns, include
from cratis_i18n.utils import localize_url as _
from .views import ProfileView

urlpatterns = patterns('',

                       url(_(r'^accounts/profile/$'),
                           ProfileView.as_view(), name='cratis_profile__main'),


                       # url(_(r'^profile/address/$'),
                       #     ProfileAddressView.as_view(
                       #     ), name='profile_address'),
                       # url(_(r'^profile/edit/$'),
                       #     ProfileEditView.as_view(), name='profile_edit'),
                       # url(_(r'^profile/edit/email/$'),
                       #     ProfileEmailEditView.as_view(
                       #     ), name='profile_edit_email'),
                       #
                       #
                )
