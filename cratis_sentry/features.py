from cratis.features import Feature


class Sentry(Feature):


    def __init__(self, dsn=None):
        super(Sentry, self).__init__()

        self.dsn = dsn

    def configure_settings(self):

        self.settings.RAVEN_CONFIG = {'dsn': self.dsn}
        self.append_apps(['raven.contrib.django.raven_compat'])