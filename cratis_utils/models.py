from random import randrange

from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.template import Context
from django.conf import settings
from django.utils import translation
from modeltranslation.utils import get_language


def generate_code(obj_type, obj, length):
    CHARSET = '0123456789ABCDEFGHJKMNPQRSTVWXYZ'
    MAX_TRIES = 1024

    """
    Upon saving, generate a code by randomly picking LENGTH number of
    characters from CHARSET and concatenating them. If code has already
    been used, repeat until a unique code is found, or fail after trying
    MAX_TRIES number of times. (This will work reliably for even modest
    values of LENGTH and MAX_TRIES, but do check for the exception.)
    Discussion of method: http://stackoverflow.com/questions/2076838/
    """
    loop_num = 0
    unique = False
    while not unique:
        if loop_num < MAX_TRIES:
            new_code = ''
            for i in xrange(length):
                new_code += CHARSET[randrange(0, len(CHARSET))]
            if not obj_type.objects.filter(code=new_code):
                obj.code = new_code
                unique = True
            loop_num += 1
        else:
            raise ValueError("Couldn't generate a unique code.")




def notify_email(lang, template, subject, to, params, attachment=None):
    cur_lang = get_language()

    translation.activate(lang)

    htmly = get_template(template)

    d = Context(params)
    html_content = htmly.render(d)

    msg = EmailMessage(subject, html_content, settings.EMAIL_FROM, [to])
    msg.content_subtype = "html"  # Main content is now text/html

    if attachment:
        msg.attach(attachment['name'], attachment['data'], attachment['type'])

    msg.send()

    translation.activate(cur_lang)