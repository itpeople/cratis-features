# -*- coding: utf-8 -*-
import json
import re

from django.db import models
from django.db.models import TextField




class RuleType(models.Model):
    name = models.CharField(max_length=100, null=False, default="", blank=False)
    description = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return '%s' % (self.name,)

class RuleTypeValue(models.Model):
    VAR_TYPES = (
        ('number', 'Number'),
        ('bool', 'Boolean'),
        ('str', 'String'),
        ('array', 'Array'),
    )

    name = models.CharField(max_length=100, null=False, default="", blank=False)
    var_type = models.CharField(max_length=10, choices=VAR_TYPES)
    description = models.TextField(blank=True, null=True)

    class Meta:
        abstract = True

class RuleTypeValueIn(RuleTypeValue):
    rule_type = models.ForeignKey(RuleType, related_name='in_vars')

class RuleTypeValueOut(RuleTypeValue):
    rule_type = models.ForeignKey(RuleType, related_name='out_vars')


class Rule(models.Model):
    name = models.CharField(max_length=100, null=False, default="", blank=False)
    rule = TextField()

    rule_type = models.ForeignKey(RuleType, null=True, blank=False)

    def __unicode__(self):
        return '%s' % (self.name,)

    def get_js(self):

        conf = json.loads(self.rule)

        if not 'js' in conf:
            return None

        js = conf['js']

        vars_, code_ = js.split('\n\n')

        vars_ = vars_.split('\n')

        new_vars_ = []
        for item in vars_:
            v = re.sub(r'var\s+([a-zA-Z0-9_]+);', r'var \1 = data.\1;', item)
            print v
            new_vars_.append(v)

        tpl = '%s\n%s' % ('\n'.join(new_vars_), code_)

        return tpl

