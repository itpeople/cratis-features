import json
from django.http import HttpResponse
from django.views.generic import TemplateView
from .models import RuleType


def load_rule(id):
    rule_type = RuleType.objects.get(pk=id)
    data = {
        'name': rule_type.name,
        'description': rule_type.description,
        'fields': {
            'in': [],
            'out': [],
        },
    }
    for field in rule_type.in_vars.all():
        data['fields']['in'].append({
            'name': field.name,
            'var_type': field.var_type,
            'description': field.description,
        })
    for field in rule_type.out_vars.all():
        data['fields']['out'].append({
            'name': field.name,
            'var_type': field.var_type,
            'description': field.description,
        })
    return data


def rule_data(request, id):
    data = load_rule(id)
    return HttpResponse(json.dumps(data), content_type="application/json")


class RulePreviewView(TemplateView):
    template_name = 'cratis_rules.html'

    def get_context_data(self, **kwargs):
        context = super(RulePreviewView, self).get_context_data(**kwargs)

        context['rule'] = load_rule(kwargs['id'])

        return context