# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Rules'
        db.create_table(u'cratis_rules_rules', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
            ('title', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
            ('rule', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'cratis_rules', ['Rules'])


    def backwards(self, orm):
        # Deleting model 'Rules'
        db.delete_table(u'cratis_rules_rules')


    models = {
        u'cratis_rules.rules': {
            'Meta': {'object_name': 'Rules'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'rule': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        }
    }

    complete_apps = ['cratis_rules']