# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'RuleTypeValueIn.name'
        db.add_column(u'cratis_rules_ruletypevaluein', 'name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100),
                      keep_default=False)

        # Adding field 'RuleTypeValueIn.var_type'
        db.add_column(u'cratis_rules_ruletypevaluein', 'var_type',
                      self.gf('django.db.models.fields.CharField')(default='number', max_length=10),
                      keep_default=False)

        # Adding field 'RuleTypeValueIn.description'
        db.add_column(u'cratis_rules_ruletypevaluein', 'description',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'RuleTypeValueOut.name'
        db.add_column(u'cratis_rules_ruletypevalueout', 'name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100),
                      keep_default=False)

        # Adding field 'RuleTypeValueOut.var_type'
        db.add_column(u'cratis_rules_ruletypevalueout', 'var_type',
                      self.gf('django.db.models.fields.CharField')(default='number', max_length=10),
                      keep_default=False)

        # Adding field 'RuleTypeValueOut.description'
        db.add_column(u'cratis_rules_ruletypevalueout', 'description',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'RuleTypeValueIn.name'
        db.delete_column(u'cratis_rules_ruletypevaluein', 'name')

        # Deleting field 'RuleTypeValueIn.var_type'
        db.delete_column(u'cratis_rules_ruletypevaluein', 'var_type')

        # Deleting field 'RuleTypeValueIn.description'
        db.delete_column(u'cratis_rules_ruletypevaluein', 'description')

        # Deleting field 'RuleTypeValueOut.name'
        db.delete_column(u'cratis_rules_ruletypevalueout', 'name')

        # Deleting field 'RuleTypeValueOut.var_type'
        db.delete_column(u'cratis_rules_ruletypevalueout', 'var_type')

        # Deleting field 'RuleTypeValueOut.description'
        db.delete_column(u'cratis_rules_ruletypevalueout', 'description')


    models = {
        u'cratis_rules.rule': {
            'Meta': {'object_name': 'Rule'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'rule': ('django.db.models.fields.TextField', [], {}),
            'rule_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cratis_rules.RuleType']", 'null': 'True'})
        },
        u'cratis_rules.ruletype': {
            'Meta': {'object_name': 'RuleType'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        u'cratis_rules.ruletypevaluein': {
            'Meta': {'object_name': 'RuleTypeValueIn'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'rule_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'in_vars'", 'to': u"orm['cratis_rules.RuleType']"}),
            'var_type': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'cratis_rules.ruletypevalueout': {
            'Meta': {'object_name': 'RuleTypeValueOut'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'rule_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'out_vars'", 'to': u"orm['cratis_rules.RuleType']"}),
            'var_type': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        }
    }

    complete_apps = ['cratis_rules']