from __future__ import unicode_literals
import json
import logging

import requests


from cratis_erply.api_utils import erply_error


class AuthFailed(Exception):
    pass


class RequestFailed(Exception):
    pass


class NoRecords(Exception):
    pass


def erply_http_request(server, data):

    data__json = requests.post(server, data=data).json()
    return data__json


class ErplyClient(object):

    """
    Erply client is erply api implementation.
    """

    session = None

    def erplyApiAuth(self):
        self.session = None
        r = self.fetch_one(
            'verifyUser', {'username': self.user, 'password': self.password})
        self.session = r['sessionKey']

    def __init__(self, user, password, client_code, server, request_method=erply_http_request):

        self.request_method = request_method

        self.client = client_code
        self.server = server

        self.user = user
        self.password = password

        self.erplyApiAuth()

    def _request(self, request, data=None, limit=None):

        data = data or {}

        data['request'] = request
        data['clientCode'] = self.client

        if self.session:
            data['sessionKey'] = self.session

        def _fetch(page, records_generated=0):

            data['pageNo'] = page
            result = self.request_method(self.server, data)

            if result['status']['responseStatus'] == 'error':
                # session expired -> reconnect
                if 1054 == result['status']['errorCode']:
                    self.erplyApiAuth()
                    self._request(request, data, limit)
                    return

                if 'errorField' in result['status']:
                    field = result['status']['errorField']
                else:
                    field = None

                if int(result['status']['errorCode']) == 1002:
                    raise RequestFailed(
                        '********************** Erply request limit is out *****************************')

                raise RequestFailed('Error during request: %s fields: %s' % (
                    erply_error(result['status']['errorCode']), field))

            if result['status']['recordsTotal'] == 0 and (not 'records' in result or len(result['records']) == 0):
               raise NoRecords
            if not 'records' in result:
                logging.error('Unexpeced result: %s' % json.dumps(result))
                raise Exception('Unexpected result')

            for record in result['records']:
                records_generated += 1
                yield record

                if limit and records_generated >= limit:
                    return

            if result['status']['recordsInResponse'] > 0 and records_generated < result['status']['recordsTotal']:
                for record in _fetch(page + 1):
                    yield record

        return _fetch(1)  # return generator

    def fetch_one(self, request, query):
        try:
            return next(self._request(request, query, 1))
        except NoRecords:
            return None

    def fetch_many(self, request, query=None, limit=None):
        try:
            return [x for x in self._request(request, query, limit)]
        except NoRecords:
            return []

    def login(self, user, password):
        return self.fetch_one('verifyCustomerUser', {
            'username': user,
            'password': password
        })

    def user_exists(self, email):
        return not self.get_user(email=email) is None

    def get_country_list(self):

        groups = self.fetch_many('getCustomerGroups')

        return [(x['customerGroupID'], x['name']) for x in groups if x['parentID'] == '15']

    def sync_erply_to_user(self, erply, email):
        from .models import User

        User.objects.filter(email=email).update(last_name=erply['lastName'],
                                                company_reg_nr=erply['code'],
                                                company_name=erply[
                                                    'companyName'],
                                                first_name=erply['firstName'])

    def sync_user_to_erply(self, data):
        r = self.fetch_one('saveCustomer', data)
        return self.get_user(_id=r['customerID'])

    def get_user(self, _id=None, email=None, code=None):

        if _id:
            query = {'customerID': _id}
        elif email:
            query = {'searchName': email}
        elif code:
            query = {'code': email}
        else:
            return ValueError('_id or email should be set')

        record = self.fetch_one('getCustomers', query)
        if record:
            self.sync_erply_to_user(record, email)

        return record

    def sync_cart(self, cart_id, user=None):

        from cratis_shop.models import Order


        price = 0
        count = 0
        product_list = ''
        data = {}
        r = {}

        if user.is_authenticated():
            order = Order.objects.get(order_cookies=cart_id)
            user = self.get_user(email=user.email)

            try:
                number = int(cart_id)
                sales_document = self.fetch_one(
                    'getSalesDocuments', {'number': number})
            except Exception, e:
                sales_document = None

            if not sales_document:
                data = {}
                data['customerID'] = user['customerID']
                data['invoiceNo'] = number
                data['type'] = 'ORDER'
                sales_document = self.fetch_one('saveSalesDocument', data)

            '''
            for product in order.get_products():
                sales_document['productID'] = product.product_id
                sales_document['amount'] = product.count
                sales_document['price'] = float(product.count * product.price)
                sales_document['vatrateID'] = 1
                sales_document = self.fetch_one('saveSalesDocument', sales_document)
            '''
        return r

    def create_address(self, owner_id, street=None, city=None, post=None, state=None, country=None):
        data = {}
        data['typeID'] = 1
        data['ownerID'] = owner_id
        data['street'] = street
        data['city'] = city
        data['postalCode'] = post
        data['state'] = state
        data['country'] = country
        return self.fetch_one('saveAddress', data)

    def get_address(self, owner_id):
        data = {}
        data['typeID'] = 1
        data['ownerID'] = owner_id
        return self.fetch_many('getAddresses', data)

    def create_user(self, email, password, first_name, last_name, companyName=None, companyRegNr=None, vatNumber=None, **kwargs):

        company = None

        if companyRegNr:
            company = self.get_user(code=companyRegNr)
            if not company:
                company = self.create_company(companyName, companyRegNr, vatNumber)

        try:
            user = self.get_user(email=email)
        except Exception, e:
            user = None

        data = {
            'firstName': first_name, 'lastName': last_name, 'username': email, 'email': email,
            'password': password, 'code': companyRegNr, 'companyName': companyName}

        if company:
           data['companyID'] = company['customerID']

        data.update(kwargs)

        if user:
            data['customerID'] = user['customerID']

        r = self.fetch_one('saveCustomer', data)

        return self.get_user(_id=r['customerID'])

    def create_company(self, companyName, companyRegNr, vatNumber):
        r = self.fetch_one('saveCustomer', {
            'companyName': companyName,
            'code': companyRegNr,
            'vatNumber': vatNumber
        })

        return self.get_user(_id=r['customerID'])


def get_client():
    """
    :type: ErplyClient
    """

    return inject.instance('erply')
