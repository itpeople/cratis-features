import pytest
from cratis_erply.api import get_client
from cratis_erply.api_mock import erply_mock_request


def test_nothing_implemented():
    with pytest.raises(NotImplementedError):
        erply_mock_request('', {'request': 'Ez'})


def test_always_success_auth_return_some_session():
    res = erply_mock_request('', {'request': 'verifyUser'})
    assert 'sessionKey' in res['records'][0]


def test_user_exist():
    res = erply_mock_request('', {'request': 'getCustomers', 'searchName': 'user@exists'})
    assert res['status']['recordsTotal'] == 1
