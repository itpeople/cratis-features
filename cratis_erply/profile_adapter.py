from cratis_erply.api import get_client


class ErplyProfileAdapter(object):

    def save_address(self, user, data):

        data['ownerID'] = user.erply_customer_id

        if not 'typeID' in data:
            data['typeID'] = 1

        return get_client().fetch_one('saveAddress', data)

    def delete_address(self, user, data):
        data['ownerID'] = user.erply_customer_id
        return get_client().fetch_one('deleteAddress', data)

    def get_addresses(self, user):
        addresses = []

        for address in get_client().fetch_many('getAddresses', {
            'ownerID': user.erply_customer_id
        }):
            address['title'] = '%s, %s. %s' % (address['country'], address['city'], address['street'])
            addresses.append(address)

        return addresses




