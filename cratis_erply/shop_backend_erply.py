from cratis_erply.api import get_client
from cratis_shop.shop.api import ShopBackend
from django.core.cache import cache
from cratis_erply.api import get_client
import easy_thumbnails
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.processors import background, scale_and_crop, colorspace
import os
import requests
import settings


class ErplyProduct(object):
    def __init__(self, data, *args, **kwargs):
        super(ErplyProduct, self).__init__(*args, **kwargs)

        self.id = data['productID']
        self.raw_data = data



def download_file(url, filename):
    local_filename = filename or url.split('/')[-1]
    # NOTE the stream=True parameter
    r = requests.get(url, stream=True)
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
                f.flush()
    return local_filename


def download_image(_type, url, _id, image_id, sizes):

    print url

    original_file = '%(root)s/%(type)s/%(id)s_%(image_id)s.jpg' % {
        'root': settings.MEDIA_ROOT,
        'type': _type,
        'id': _id,
        'image_id': image_id
    }
    relative_name = '%(type)s/thumbs/%(id)s_%(image_id)s' % {
        'type': _type,
        'id': _id,
        'image_id': image_id
    }

    if not os.path.exists(os.path.dirname(original_file)):
        os.makedirs(os.path.dirname(original_file))

    if not os.path.exists(original_file):
        download_file(url, original_file)

    generated_sizes = {}
    with open(original_file) as picture:
        for size_name, size in sizes.items():
            thumbnailer = get_thumbnailer(picture, relative_name=relative_name)
            thumbnailer.thumbnail_processors = [colorspace, scale_and_crop, background]
            thumb = thumbnailer.get_thumbnail(size)

            generated_sizes[size_name] = '%s/%s' % (settings.MEDIA_URL, thumb)

    return generated_sizes




class CratisErplyBackend(ShopBackend):
    def fetch_products(self, changed_since=None):

        for data in get_client().fetch_many('getProducts', {
            'recordsOnPage': 1000,
            'getMatrixVariations': 1,
            # 'getReplacementProducts': 1,
            # 'getStockInfo': 1,
            # 'getRelatedProducts': 1,
            # 'getRelatedFiles': 1,
            'getAllLanguages': 1,
            'active': 1,
            'displayedInWebshop': 1,
            'getParameters': 1,
            'type': 'MATRIX,PRODUCT',
            'includeMatrixVariations': 1,
        }):
            data['hasVariations'] = data['type'] == 'MATRIX'
            data['isVariation'] = 'variationDescription' in data

            # if 'images' in data:
            yield ErplyProduct(data)

    def fetch_product(self, changed_since=None, id=0):
        data = get_client().fetch_one('getProducts', {'productIDs': id, 'getParameters': 1})
        return ErplyProduct(data)

    def fetch_categories(self, changed_since=None):
        groups = get_client().fetch_many('getProductGroups', {
            'getAllLanguages': 1
        })

        # return list(groups)

        cats = [g for g in groups if g['showInWebshop'] != '0']

        return cats

    def fetch_price_lists(self):
        lists = get_client().fetch_many('getPriceLists', {}, 300)

        lists = [l for l in lists if l['name'].startswith('grandex_')]

        return lists

    def fetch_currency_list(self):
        list = get_client().fetch_many('getCurrencies', {}, 300)

        return list

    def fetch_product_variations(self, changed_since=None):
        return list(get_client().fetch_many('getMatrixDimensions', {'recordsOnPage': 100, }))

    def fetch_parameter(self, changed_since=None, id=0):
        params = self.fetch_parameters()
        for param in params:
            if param['parameterID'] == id:
                return param
        return None

    def fetch_parameters(self, changed_since=None):
        # params = cache.get('erply_params')
        # if not params:
        params = list(get_client().fetch_many('getParameters'))
            # cache.set('erply_params', params)

        return params


    def download_product_pictures(self, product, sizes):
        all_pics = []
        if 'images' in product:
            for image in product['images']:
                generated_sizes = download_image('product', image['fullURL'], product['productID'], image['pictureID'], sizes)
                all_pics.append({'sizes': generated_sizes, 'id': image['pictureID']})

        return all_pics


    def download_category_pictures(self, category, sizes):

        all_pics = []
        if 'images' in category:
            for image in category['images']:
                generated_sizes = download_image('category', image['largeURL'], category['productGroupID'], image['pictureID'], sizes)
                all_pics.append({'sizes': generated_sizes, 'id': image['pictureID']})

        return all_pics