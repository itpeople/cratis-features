from cratis.features import Feature
from cratis_erply.api import ErplyClient


class ErplyIntegration(Feature):

    def __init__(self, server=None, client_code=None, username=None, password=None):
        super(ErplyIntegration, self).__init__()

        self.server = server
        self.client_code = client_code
        self.username = username
        self.password = password


    def configure_settings(self):
        if not hasattr(self.settings, 'ERPLY_API_DEBUG'):
            self.settings.ERPLY_API_DEBUG = False


        self.append_apps(['cratis_erply'])

    def configure_services(self, binder):

        from cratis_erply.order_storage import ErplyOrderStorage
        from cratis_erply.profile_adapter import ErplyProfileAdapter

        binder.bind_to_constructor('erply', lambda: ErplyClient(
            user=self.username,
            password=self.password,
            client_code=self.client_code,
            server=self.server
        ))

        binder.bind('profile_adapter', ErplyProfileAdapter())
        binder.bind('order_storage', ErplyOrderStorage())


class ErplyAuth(Feature):
    ERPLY_ADMIN_GROUP = None

    def __init__(self):
        super(ErplyAuth, self).__init__()

    def configure_settings(self):
        self.append_apps(['cratis_erply'])

        self.settings.AUTHENTICATION_BACKENDS = ('cratis_erply.auth.ErplyAuthBackend', )
        self.settings.AUTH_USER_MODEL = "cratis_erply.User"
