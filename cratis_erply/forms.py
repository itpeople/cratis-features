from __future__ import unicode_literals, absolute_import

from cratis_erply.api import get_client

from django.utils.translation import ugettext_lazy as _
from django.forms.util import ErrorList

from .models import User
from django import forms


class ProfileEditForm(forms.ModelForm):
    email = forms.CharField(required=True)
    first_name = forms.CharField(
        max_length=155, label=_('First name'), required=False)
    last_name = forms.CharField(
        max_length=155, label=_('Last name'), required=False)
    company_name = forms.CharField(max_length=155, label=_('Company name'),
                                   required=False, widget=forms.TextInput(attrs={'readonly': 'readonly'}))
    company_reg_nr = forms.CharField(
        max_length=155, label=_('Company reg.nr.'),
        required=False, widget=forms.TextInput(attrs={'readonly': 'readonly'}))

    company_vat_nr = forms.CharField(
        max_length=155, label=_('Company vat.nr.'),
        required=False, widget=forms.TextInput(attrs={'readonly': 'readonly'}))

    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name',
                  'company_name', 'company_reg_nr']

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(ProfileEditForm, self).__init__(*args, **kwargs)


class ProfileEditEmailForm(forms.ModelForm):
    email = forms.CharField(required=True)

    class Meta:
        model = User
        fields = ['email']

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(ProfileEditEmailForm, self).__init__(*args, **kwargs)
