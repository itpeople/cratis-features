""" Model Plot Data Handler"""
from .simple import SimpleDataSource



class ModelDataSource(SimpleDataSource):
    def __init__(self, queryset, fields=None, filter=None):
        self.queryset = queryset
        if not filter:
            filter = lambda field, val: val

        self.filter = filter

        if fields:
            self.fields = fields
        else:
            self.fields = [el.name for el in self.queryset.model._meta.fields]
        self.data = self.create_data()


    def get_field_values(self, row, fields):
        data = []
        for field in fields:
            val = getattr(row, field)
            val = self.filter(field, val)
            data.append(val)
        return data

    def create_data(self):
        data = [self.fields]
        for row in self.queryset:
            data.append(self.get_field_values(row, self.fields))
        return data
