
from cratis.features import Feature
from django.conf.urls import patterns, include, url

import forms_builder.forms.urls


class Forms(Feature):

    def configure_settings(self):

        self.append_apps([
            'cratis_forms',
            'forms_builder.forms',
        ])

        self.settings.FORMS_BUILDER_EDITABLE_SLUGS = True

    def configure_urls(self, urls):
        urls += patterns('',

            url(r'^forms/', include(forms_builder.forms.urls)),
        )


