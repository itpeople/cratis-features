from django.conf.urls import *

urlpatterns = patterns(
    "cratis_forms.views",
    url(r"(?P<slug>.*)/sent/$",
        "form_sent",
        name="form_sent"),
)
