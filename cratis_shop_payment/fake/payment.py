from django.core.urlresolvers import reverse
from voluptuous import Schema
from cratis_shop_payment.common import Payment

__author__ = 'alex'



class FakePayment(Payment):

    def require_payment(self):
        return True

    def payment_url(self):
        return reverse('fake_payment_start', kwargs={'method': self.model.slug})




