from cratis_shop_payment.fake.views import FakeStart
from django.conf.urls import patterns, url
from django.views.decorators.cache import never_cache

urlpatterns = patterns('cratis_shop_payment.dibs.views',
    url(r'^(?P<method>[^\\]+)/start', never_cache(FakeStart.as_view()), name='fake_payment_start'),
)


