from cratis_filer.serializers import ThumbnailImageField, Size
from cratis_shop_payment.models import PaymentMethod, PaymentOrder

from rest_framework import serializers


class PaymentMethodInfoSerializer(serializers.Serializer):

    payment_url = serializers.CharField()
    require_payment = serializers.BooleanField()

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class PaymentOrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentOrder

        # fields = ('id', 'name', 'info', 'logo')



class PaymentMethodSerializer(serializers.ModelSerializer):

    info = PaymentMethodInfoSerializer()

    logo = ThumbnailImageField({
        'thumb1': Size(100, 100),
        'thumb2': Size(130, 60),
    })

    class Meta:
        model = PaymentMethod

        exclude = ('config',)


