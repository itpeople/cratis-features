from django.core.urlresolvers import reverse
from voluptuous import Required, Schema
from cratis_shop_payment.common import Payment


class PaymillPayment(Payment):

    def schema(self):
        return Schema({}, required=False)

    def require_payment(self):
        return True



