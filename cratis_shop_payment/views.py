# coding=utf-8
import json
from cratis_shop_payment.serializers import PaymentMethodSerializer
from django.shortcuts import get_object_or_404
from django.utils.translation import get_language

from django.views.generic import TemplateView, View

from djangular.views.mixins import JSONResponseMixin, allow_remote_invocation
from cratis_shop_payment.models import PaymentOrder
from cratis_shop_regions.context import get_region
from cratis_shop_payment.models import PaymentMethod

from django.utils.translation import ugettext_lazy as _
from main.common import notify_email_mandrill


class PaymentView(View):

    def load_method(self, method):
        return  get_object_or_404(PaymentMethod, slug=method)


class CheckoutView(JSONResponseMixin, TemplateView):
    template_name = 'cart/checkout.html'

    # checkout_service = inject.attr('checkout_service')
    """ @type: cratis_shop_payment.service.CheckoutService """

    def po_for_order(self, order):
        po = None
        if 'cart_id' in order:
            po = PaymentOrder.objects.get(pk=order['cart_id'], user=self.request.user)

        if not po:
            po = PaymentOrder()
            po.user = self.request.user

        return po

    @allow_remote_invocation
    def save_cart(self, order):
        po = self.last_order()

        if not po:
            po = PaymentOrder()
            po.user = self.request.user

        po.body = json.dumps(order)
        po.save()

    def last_order(self):
        if self.request.user.is_anonymous():
            return None

        pos = PaymentOrder.objects.filter(user=self.request.user.id, checked_out=False).order_by('-date_created')

        if pos:
            return pos[0]

        return None

    @allow_remote_invocation
    def load_cart(self):

        pos = self.last_order()
        if pos:
            return {'exists': True, 'order': json.loads(pos.body)}

        return {'exists': False, 'order': None}

    @allow_remote_invocation
    def payment_methods(self):
        return PaymentMethodSerializer(PaymentMethod.objects.filter(enabled=True), many=True).data

    @allow_remote_invocation
    def pay(self, in_data):
        po = self.last_order()
        po.body = json.dumps(in_data['order'])
        po.save()

        # needed for payments
        self.request.session['order_id'] = po.id
        self.request.session['payment_done_url'] = in_data.get('payment_done_url', '/')
        self.request.session['payment_cancel_url'] = in_data.get('payment_cancel_url', '/')

    @allow_remote_invocation
    def checkout(self, in_data):
        po = self.last_order()
        po.body = json.dumps(in_data['order'])
        po.save()

        po.checkout()

        from main.common import notify_email
        from shop.views import invoice_pdf


        pdf = invoice_pdf(po, self.request)

        notify_email_mandrill(get_language(), 'order-confirm-offline', _(u'Ваша покупка оформлена'), self.request.user.email, {
            'osum': po.sum,
            'user': self.request.user
        }, attachment={
            'name': 'invoice.pdf',
            'data': pdf,
            'type': 'application/pdf'
        })

    def get_context_data(self, **kwargs):
        context = super(CheckoutView, self).get_context_data(**kwargs)

        return context
