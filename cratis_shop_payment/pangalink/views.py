import hashlib
import re

from datetime import datetime

from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

from cratis_shop_payment.common import log_payment
from cratis_shop_payment.crypto import ssl_sign, load_pem_private_key, load_pem_public_key, ssl_verify, load_pem_cert_public_key
from cratis_shop_payment.models import PaymentOrder
from cratis_shop_payment.views import PaymentView


__author__ = 'alex'

def sha1(string):
    return hashlib.sha1(string).hexdigest()

def md5(string):
    return hashlib.md5(string).hexdigest()


class PangalinkCallback(PaymentView):

    def bad_payment(self, request):
        if 'fail_url' in request.session:
            return HttpResponseRedirect(request.session['fail_url'])
        else:
            return HttpResponseRedirect('/')

    def post(self, request, *args, **kwargs):

        method = self.load_method(kwargs['method'])
        settings = method.behavior().config

        log_payment(request, 'pangalink_' + kwargs['method'], 'pay_callback')

        if request.POST['VK_SERVICE'] != '1111':
            log_payment(request, 'pangalink', 'pay_cancel')

            return self.bad_payment(request)

        mac_data = ''
        for field in ['VK_SERVICE', 'VK_VERSION', 'VK_SND_ID',
                'VK_REC_ID', 'VK_STAMP', 'VK_T_NO', 'VK_AMOUNT', 'VK_CURR',
                'VK_REC_ACC', 'VK_REC_NAME', 'VK_SND_ACC', 'VK_SND_NAME',
                'VK_REF', 'VK_MSG', 'VK_T_DATETIME']:
            v = request.POST[field]
            vlen = len(v)
            mac_data += str(vlen).zfill(3) + v

        public_key = load_pem_cert_public_key(settings['pubkey'])

        if ssl_verify(mac_data.encode('utf-8'), request.POST['VK_MAC'].decode('base64'), public_key):

            ecuno = request.POST['VK_STAMP']

            order_id = int(ecuno) - 100000

            log_payment(request, 'pangalink', 'pay_success')

            order = PaymentOrder.objects.get(pk=order_id)
            order.paid = True
            order.save()

            if hasattr(order, 'ticcet_order'):
                for ticcet_order in order.ticcet_order.all():
                    ticcet_order.paid = True
                    ticcet_order.save()

            if 'success_url' in request.session:
                return HttpResponseRedirect(request.session['success_url'])

            return HttpResponseRedirect(reverse('payment_done'))

        else:

            print request.POST['VK_MAC']
            #return self.bad_payment(request)



class PangalinkCancel(PaymentView):
    def post(self, request, *args, **kwargs):
        log_payment(request, 'pangalink', 'pay_cancel')

        return HttpResponseRedirect(reverse('orders_checkout'))

class PangalinkStart(PaymentView):

    def get(self, request, *args, **kwargs):

        method = self.load_method(kwargs['method'])
        settings = method.behavior().config

        order_id = request.GET.get('id')
        if not order_id:
            order_id = request.session['order_id']

        po = PaymentOrder.objects.get(pk=order_id)

        feedbackurl = 'http://' + request.META['HTTP_HOST'] + reverse('pangalink_payment_callback',
                                                                      kwargs={'method': method.slug})

        feedbackurl = str(feedbackurl).strip()

        order_total = po.sum
        order_id = po.id


        oid = settings['id']

        ecuno = str(order_id + 100000)
        eamount = str(order_total)

        # padding
        # feedbackurl = feedbackurl.ljust(128)
        ecuno = ecuno.zfill(12)
        eamount = eamount.zfill(12)

        data = {
            'VK_SERVICE': '1012',
            'VK_VERSION': '008',
            'VK_SND_ID': oid,
            'VK_STAMP': ecuno,
            # 'VK_STAMP': "12345",
            'VK_AMOUNT': eamount,
            # 'VK_AMOUNT': "150",
            'VK_CURR': 'EUR',
            'VK_ACC': settings['account'],
            'VK_NAME': settings['owner'],
            'VK_REF': '',
            'VK_MSG': 'Order id ' + str(order_id),
            # 'VK_MSG': "Torso Tiger",
            'VK_RETURN': feedbackurl,
            # 'VK_RETURN': "http://dev.ticcet.com:3480/project/XcrujFk2lk7nQo7W?payment_action=success",
            'VK_CANCEL': feedbackurl,
            # 'VK_CANCEL': "http://dev.ticcet.com:3480/project/XcrujFk2lk7nQo7W?payment_action=cancel",

            'VK_DATETIME': datetime.now().strftime('%Y-%m-%dT%H:%M:%S+0200'),
            'VK_ENCODING': 'utf-8',
        }

        mac_data = ''
        for field in ['VK_SERVICE', 'VK_VERSION', 'VK_SND_ID',
                'VK_STAMP', 'VK_AMOUNT', 'VK_CURR', 'VK_REF', 'VK_MSG', 'VK_RETURN', 'VK_CANCEL', 'VK_DATETIME']:
            v = data[field]
            vlen = len(v)
            mac_data += str(vlen).zfill(3) + v

        print mac_data.encode('utf-8')

        signed = ssl_sign(mac_data.encode('utf-8'), load_pem_private_key(settings['pkey']))

        data['VK_MAC'] = re.sub('\s+', '', signed.encode('base64'))

        log_payment(request, 'pangalink', 'pay_start', data)

        return render(request, 'payment/post_redirect.html', {'url': settings['url'], 'fields': data.items()})

        #'https://pangalink.net/banklink/ec?' + send_data
        #return HttpResponseRedirect('?' + send_data)

