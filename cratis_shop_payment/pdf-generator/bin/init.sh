#!/bin/sh
### BEGIN INIT INFO
# Provides:          shops-reporter
# Required-Start:    $local_fs $remote_fs $network $syslog
# Required-Stop:     $local_fs $remote_fs $network $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# X-Interactive:     true
# Short-Description: Start/stop shops-reporter server
### END INIT INFO
 
case $1 in
    start)
        echo "Starting shops-reporter ..."
        if [ ! -f /home/ubuntu/reporter/pid ]; then
            nohup java -jar /home/ubuntu/reporter/shops-reporter_server.jar /home/ubuntu/reporter 2>> /home/ubuntu/reporter/reporter.log >> /home/ubuntu/reporter/reporter.log &
            echo $! > /home/ubuntu/reporter/pid
            echo "shops-reporter started ..."
        else
            echo "shops-reporter is already running ..."
        fi
    ;;
    stop)
        if [ -f /home/ubuntu/reporter/pid ]; then
            PID=$(cat /home/ubuntu/reporter/pid);
            echo "Stopping shops-reporter ..."
            kill $PID;
            echo "shops-reporter stopped ..."
            rm /home/ubuntu/reporter/pid
        else
            echo "shops-reporter is not running ..."
        fi
    ;;
    restart)
        if [ -f /home/ubuntu/reporter/pid ]; then
            PID=$(cat /home/ubuntu/reporter/pid);
            echo "Stopping shops-reporter ...";
            kill $PID;
            echo "shops-reporter stopped ...";
            rm /home/ubuntu/reporter/pid
 
            echo "Starting shops-reporter ..."
            nohup java -jar /home/ubuntu/reporter/shops-reporter_server.jar /home/ubuntu/reporter 2>> /home/ubuntu/reporter/reporter.log >> /home/ubuntu/reporter/reporter.log &
            echo $! > /home/ubuntu/reporter/pid
            echo "shops-reporter started ..."
        else
            echo "shops-reporter is not running ..."
        fi
    ;;
esac