package maksa.shops.reporter;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import maksa.shops.reporter.domain.Invoice;
import maksa.shops.reporter.domain.InvoiceRow;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 */
public class Main {


    public static boolean working = true;

    public static String hash(String text) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(text.getBytes());

            byte[] digest = md.digest();

            return byteArrayToHexString(digest);

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
    public static String byteArrayToHexString(byte[] b) {
        String result = "";
        for (int i=0; i < b.length; i++) {
            result +=
                    Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
        }
        return result;
    }

    public static void main(String [] args) throws InterruptedException {
        process(args[0].trim());
        System.exit(0);
    }

    private static void process(String rawData) throws InterruptedException {
        MailService mailService = new MailService();

        BasicAWSCredentials credentials = new BasicAWSCredentials("AKIAI4YBC5YFS6Q4IXDA", "cT7zHx2r8j9mk7A6KVh7272EYmvWMOnv8kdoVcDY");

        BASE64Decoder decoder = new BASE64Decoder();

        try {
            byte[] decodedBytes = decoder.decodeBuffer(rawData);
            String data = new String(decodedBytes);

            Invoice invoice = new Invoice();
            JSONObject json = new JSONObject(data);
            invoice.setShippingAddress(json.getString("shippingAddress"));
            invoice.setShippingReturnAddress(json.getString("shippingReturnAddress"));
            invoice.setBillAddress(json.getString("billAddress"));

            invoice.setInfoHeader(json.getString("infoHeader"));
            invoice.setInfoFooterCol1(json.getString("infoFooterCol1"));
            invoice.setInfoFooterCol2(json.getString("infoFooterCol2"));
            invoice.setInfoFooterCol3(json.getString("infoFooterCol3"));

            invoice.setCompanyName(json.getString("companyName"));
            invoice.setAdminEmail(json.getString("adminEmail"));
            invoice.setEmail(json.getString("email"));
            invoice.setFromEmail(json.getString("fromEmail"));

            invoice.setTax(new BigDecimal(json.getString("tax")));
            invoice.setTaxTotal(new BigDecimal(json.getString("taxTotal")));
            invoice.setTotal(new BigDecimal(json.getString("total")));
            invoice.setSubtotal(new BigDecimal(json.getString("subtotal")));

            invoice.setId(json.getInt("id"));

            invoice.setDate(json.getString("date"));
            invoice.setLogo(decoder.decodeBuffer(json.getString("logo")));

            JSONArray rows = json.getJSONArray("rows");
            for(int i = 0; i < rows.length(); i++) {
                JSONObject row = (JSONObject) rows.get(i);
                invoice.addRow(new InvoiceRow(
                        row.getInt("id"),
                        row.getString("title"),
                        row.getInt("qty"),
                        new BigDecimal(row.getString("unitPrice")),
                        new BigDecimal(row.getString("totalPrice")),
                        new BigDecimal(row.getString("deliveryPrice"))
                ));
            }

            JSONObject texts = json.getJSONObject("texts");
            Iterator keys = texts.keys();
            while(keys.hasNext()) {
                String key = (String)keys.next();
                invoice.getTexts().put(key, texts.getString(key));
            }

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            getReportService().build(invoice, stream);
            byte[] invoicePdf = stream.toByteArray();

            stream.reset();
            getReportService().buildShippingList(invoice, stream);
            byte[] shippingListPdf = stream.toByteArray();


            mailService.sendMessages(invoice, invoicePdf, shippingListPdf);

            uploadPdfToS3(credentials, invoicePdf,
                    "invoice-" + generateFilenameForInvoice(invoice) + ".pdf");

            uploadPdfToS3(credentials, shippingListPdf,
                    "shipping-list-" + generateFilenameForInvoice(invoice) + ".pdf");

            System.out.println("Finished generating invoice " + invoice.getId());

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.exit(1);
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.exit(1);
        }
    }

    private static String generateFilenameForInvoice(Invoice invoice) {
        String s = invoice.getId().toString();
        return hash(s + "hoho12") + "-" + hash(s + "hoho21");
    }

    private static void uploadPdfToS3(BasicAWSCredentials credentials, byte[] pdfBytes, String fileName) {
        ByteArrayInputStream slis = new ByteArrayInputStream(pdfBytes);
        ObjectMetadata metadata = new ObjectMetadata();

        metadata.setContentLength(pdfBytes.length);
        metadata.setContentType("application/pdf");

        // start aws upload
        AmazonS3Client client = new AmazonS3Client(credentials);
        PutObjectResult putResult = client.putObject(
                new PutObjectRequest("maksa-shops-invoice", fileName, slis, metadata).withCannedAcl(CannedAccessControlList.PublicRead)
        );
    }


    public static ReportService getReportService()
    {
        return new ReportService();
    }
}
