import json
from paypal.pro.signals import payment_was_successful
from cratis_shop_payment.models import PaymentOrder



def on_payment_success(sender, **kwargs):
    amt = kwargs['paymentrequest_0_amt']
    pk = kwargs['inv']

    inject.instance('checkout_service').payment_success(PaymentOrder.objects.get(pk=pk), amt)

payment_was_successful.connect(on_payment_success)
