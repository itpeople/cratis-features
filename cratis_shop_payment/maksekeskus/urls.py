from django.conf.urls import patterns, url
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
from cratis_shop_payment.maksekeskus.views import MaksekeskusStart, MaksekeskusCallback


urlpatterns = patterns('cratis_shop_payment.maksekeskus.views',
    url(r'^(?P<method>[^\\]+)/start', never_cache(MaksekeskusStart.as_view()), name='maksekeskus_payment_start'),
    url(r'^(?P<method>[^\\]+)/callback', csrf_exempt(never_cache(MaksekeskusCallback.as_view())), name='maksekeskus_payment_callback'),
)



