import json


class CheckoutService(object):

    def payment_success(self, po, amount):

        order = json.loads(po.body)
        po.pay()

        return True

    def confirm_order(self, po, paid=False):

        po.checkout()

        if paid:
            po.pay()

        return True
