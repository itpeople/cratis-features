import hashlib
import json
import urllib

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404
from django.utils.encoding import smart_str
from django.utils.translation import get_language

from cratis_shop_payment.common import log_payment, get_client_ip
from cratis_shop_payment.models import PaymentMethod, PaymentOrder
from cratis_shop_payment.views import PaymentView


def md5(string):
    return hashlib.md5(string).hexdigest()


def validate_payment(request):

    settings = config_get_group('PAYMENT_DIBS')
    transaction_id = request.POST['transact']
    amount = request.POST['amount']
    currency = request.POST['currency']

    hash_data = '&transact=' + \
        smart_str(transaction_id) + '&amount=' + smart_str(
            amount) + '&currency=' + smart_str(currency)
    md5_key1 = settings['MD51']
    md5_key2 = settings['MD52']

    calculated_authkey = md5(md5_key2 + md5(md5_key1 + hash_data))
    if calculated_authkey != request.POST['authkey']:
        log_payment(request, 'dibs', 'pay_accept_error',
                    'Siganture not valid. Data: ' + hash_data + ' Key: ' + calculated_authkey)
        return False
    return True


class DibsAccept(PaymentView):

    def post(self, request, *args, **kwargs):
        log_payment(request, 'dibs', 'pay_accept')

        # skip validation
        order_id = request.POST['orderid']
        amount = int(request.POST['amount']) / 100

        inject.instance('checkout_service').payment_success(PaymentOrder.objects.get(pk=order_id), amount=amount)

        return HttpResponseRedirect(reverse('cratis_profile__main'))


        #    if not validate_payment(request):
        #        return HttpResponse('Bad signature. Can not accpet payment.', None, 403)
        #

        #
        #    amount = request.POST['amount']
        #
        #    order = Order.objects.get(order_id)
        #    if (order.get_detalisation()['full_total'] * 100) != amount:
        #        log_payment(request, 'dibs', 'pay_accept_error', 'Order amount do not match: %s not equals %s' % (order.get_detalisation()['full_total'] * 100, amount))

        #    return HttpResponse('pay_accept')


class DibsCancel(PaymentView):

    def post(self, request, *args, **kwargs):
        log_payment(request, 'dibs', 'pay_cancel')

    #    order_id = request.POST['orderid']
    #    order = Order.objects.get(order_id)
    #    order.mark_paid()

        return HttpResponseRedirect(reverse('orders_checkout'))


class DibsCallback(PaymentView):

    def post(self, request, *args, **kwargs):
        log_payment(request, 'dibs', 'pay_callback')

        # skip validation
        order_id = request.POST['orderid']
        order = Order.objects.get(order_id)
        order.mark_paid()

        return HttpResponse('ok')


class DibsStart(PaymentView):

    def get(self, request, *args, **kwargs):

        method = self.load_method(kwargs['method'])
        settings = method.behavior().config

        payment = get_object_or_404(PaymentMethod, slug=method)
        config = payment.behavior().config

        po = PaymentOrder.objects.get(pk=request.session['order_id'])
        order = json.loads(po.body)


    #    for row in detalisation['rows']:
    #        ulink_order.items.append(UlinkOrderItem(smart_str(row['title']), '', str(row['price'])))

        # Preparing the data that we are sending to DIBS
        # Order total to be sent to DIBS must be an int specified in cents or
        # equivalent.


        success_url = reverse('cratis_profile__main')
        return_url = reverse('dibs_payment_accept', kwargs={'method': method.slug})
        cancel_url = reverse('shop_checkout')

        return_url = request.build_absolute_uri(return_url)
        cancel_url = request.build_absolute_uri(cancel_url)
        success_url = request.build_absolute_uri(success_url)


        order_total = int(float(po.sum) * 100)
        order_id = request.session['order_id']

        # Create md5 hash to make payment secure:
        md5_key = md5(settings['MD52'] +
                      md5(settings['MD51'] + 'merchant=%s&orderid=%s&currency=%s&amount=%s' % (settings['MERCHANT'], order_id, order['currency']['code'].upper(), order_total)))

        # Create the cancel and accept url, based on the request to get the host
        # and reverse to get the url.
        #        cancelurl = 'http://' + request.META['HTTP_HOST'] + reverse('satchmo_checkout-step1')
        #        accepturl = 'http://' + request.META['HTTP_HOST'] + reverse('DIBS_satchmo_checkout-success')
        #        callbackurl = 'http://' + request.META['HTTP_HOST'] + reverse('DIBS_satchmo_checkout-step4') + '?order_id=' + str(order.id)
        #


        data = [
            ('merchant', settings['MERCHANT']),
            ('amount', order_total),
            ('currency', order['currency']['code'].upper()),
            ('orderid', order_id),
            ('accepturl', return_url),
            ('cancelurl', cancel_url),
            ('callbackurl', success_url),
            ('ip', get_client_ip(request)),
            #('uniqueoid', 'yes'),
            ('lang', get_language()),
            ('md5key', md5_key),
            ('calcfee', 'yes'),
            # Currently not implemented in the flex window.
            # ('delivery1', order.ship_addressee),
            # ('delivery2', order.ship_street1),
            # ('delivery3',  order.ship_postal_code + ' ' +  order.ship_city)
            ('capturenow', 'yes'),
        ]

        log_payment(request, 'dibs', 'pay_start', data)

    #    if settings['CAPTURE']:
    #    data.append(('capturenow', 'yes'))

        if not settings['LIVE']:
            data.append(('test', 'yes'))

        send_data = urllib.urlencode(data)

        return HttpResponseRedirect('https://payment.architrade.com/paymentweb/start.action?' + send_data)
