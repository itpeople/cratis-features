from django.conf.urls import patterns, include
from cratis.features import Feature
import os


class Payment(Feature):
    def configure_settings(self):
        super(Payment, self).configure_settings()

        self.append_apps([
            "cratis_shop_payment",
            'django_ace',
            'suit_redactor'
        ])

        if hasattr(self.settings, 'PAYPAL_WPP_USER'):
            self.append_apps(['paypal.pro', "cratis_shop_payment.paypal"])


        self.settings.TEMPLATE_DIRS += (os.path.dirname(__file__) + '/fake/templates',)

    def configure_urls(self, urls):
        urls += patterns('',
                         ('', include('cratis_shop_payment.urls')),
        )


    def configure_services(self, binder):
        from cratis_shop_payment.service import CheckoutService

        binder.bind('checkout_service', CheckoutService())

