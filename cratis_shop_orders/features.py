from cratis.features import Feature


class LocalOrders(Feature):

    def configure_settings(self):
        self.append_apps(['cratis_shop_orders'])


    def create_adapter(self):
        from cratis_shop_orders.order_storage import LocalOrderStorage
        return LocalOrderStorage()

    def configure_services(self, binder):

        binder.bind_to_constructor('order_storage', self.create_adapter)
