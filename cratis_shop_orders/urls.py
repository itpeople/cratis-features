from django.conf.urls import url, patterns, include
from cratis_i18n.utils import localize_url as _
from .views import OrderView

urlpatterns = patterns(
    '',

    url(_(r'^accounts/orders/$'), OrderView.as_view(), name='cratis_shop_orders__main'),
)
