
==================
Themes
==================

Easy theming support for Django & Django cms.


.. note::

    If you use Cms() feature, Theme() feature should go after Cms(), then it will add
    registered cms templates into CMS_TEMPLATES settings variable.

