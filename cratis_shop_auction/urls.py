from cratis_shop_auction.views import FrontPageView, DealView
from django.conf.urls import url, patterns

urlpatterns = patterns('',
   url(r'^$', FrontPageView.as_view(), name='front_page'),
   url(r'^deal/(?P<deal_id>\w+)$', DealView.as_view(), name='deal'),

)
