from cratis_shop_auction.models import Deal, ProductCategory
from django.db.models import Count
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView


class FrontPageView(TemplateView):

    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        data = super(FrontPageView, self).get_context_data(**kwargs)

        data['deals'] = Deal.objects.filter(featured=True)
        data['categories'] = ProductCategory.objects.annotate(num=Count('deals'))

        return data


class DealView(TemplateView):
    template_name = 'deal.html'

    def get_context_data(self, deal_id=None, **kwargs):
        data = super(DealView, self).get_context_data(**kwargs)

        data['deal'] = get_object_or_404(Deal, pk=deal_id)

        return data