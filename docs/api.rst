
Api Detailed documentation
***************************

Feature API
===================

.. automodule:: cratis.features

    .. autoclass:: Feature
        :members:

    .. autoclass:: BaseFeature
        :members:

Settings API
===================

.. automodule:: cratis.settings

    .. autoclass:: CratisConfig
        :members:


CLI tasks
===================

.. automodule:: cratis.cli

    .. autofunction:: cratis_cmd
    .. autofunction:: cratis_init_cmd
    .. autofunction:: load_env
    .. autofunction:: _find_project_env

