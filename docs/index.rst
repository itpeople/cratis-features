
Django-cratis extension modules
**************************************

List of features
====================

.. features

.. toctree::
  :maxdepth: 3

  cratis_admin/index
  cratis_admin_suit/index
  cratis_base/index
  cratis_cms/index
  cratis_erply/index
  cratis_themes/index

.. features end

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`