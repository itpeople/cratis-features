
Getting Started
***************************

Installation
-----------------

Just use pip:

    pip install django-cratis

The only-requirements that are auto-installed:

* django>=1.4
* django-configurations - http://django-configurations.readthedocs.org/en/latest/


Running cratis
-------------------

django-cratis requires only two files:

cratis.yml
    Declares environment variables that will be passed to your application.

settings.py
    Your project configuration.

You can generate those files::

    $ cratis-init

Now, you are ready to run application::

    $ cratis runserver

.. note::
    command "cratis" is equivalent for "python manage.py", so you can do with cratis everything
    you could do with manage.py command.


Understanding structure
-------------------------

Now, before continue configuring our cratis based project we need to take a look
at files we just generated with help of *cratis-init* command.

cratis.yml
''''''''''''''''

Default content of file::

    CRATIS_APP_PATH: .
    DJANGO_CONFIGURATION: Dev
    DJANGO_SETTINGS_MODULE: settings

This file is what cratis command is looking for when you execute *cratis* command (:py:func:`cratis.cli.cratis_cmd`).

It plays two roles:

* It is **marker** that indicates where your project root is located.

  You can run cratis command also from sub-folders of project, it will find root automatically.

* It contains **environment variables** that is passed to your application.

  Environment variables are heavily used by django-configurations and django itself

Variables that are required:

CRATIS_APP_PATH
    indicates where the project root is located. Usually in same folder, where cratis.yml is, but in production
    it may be different.

    Path specified in this variable also will be added to sys.path.

DJANGO_CONFIGURATION
    indicates which configuration environment to use. See `Django Configurations <http://django-configurations.readthedocs.org/en/latest/>`_

DJANGO_SETTINGS_MODULE
    usually it is "settings", but you may specify any other.


settings.py
''''''''''''''''

Default content of file::

    # coding=utf-8
    from cratis.settings import CratisConfig


    class Dev(CratisConfig):
        DEBUG = True
        SECRET_KEY = '8qIcLsQdsbI9OmRUWWnh56Qx1bSnLc'

        FEATURES = (
            # your features here
        )


    class Test(Dev):
        pass


    class Prod(Dev):
        DEBUG = False

        FEATURES = Dev.FEATURES + (
            # your features here
        )

settings.py uses `Django Configurations <http://django-configurations.readthedocs.org/en/latest/>`_ to provide
different configurations for different environments, otherwise it is usual django settings.

The most important thing here is FEATURES. Parent class of this setting `:py:class:cratis.settings.CratisConfig`
uses this variable to find and load your features.

*The Features* allow not to manipulate your settings directly when you need to install some new application,
but delegate it to `:py:class:cratis.features.Feature` class.

Out of the box each Feature is able to:

* Manipulate INSTALLED_APPS, MIDDLEWARE_CLASSES, TEMPLATE_CONTEXT_PROCESSORS (Special convinient api)
* Have access and may change any of SETTINGS variables.
* Do startup actions required

Creating own Feature
----------------------------

Creating own feature is super-simple. Just create a new class that extends `cratis.features.Feature`.
Then override it's methods depending on your needs.

Example::

    from cratis.features import Feature
    from django.conf.urls import patterns, include


    class Hotel(Feature):

        def configure_settings(self, cls):

            self.append_apps(cls, ['hotel', 'imperavi', 'django_extensions',
                                   'newsletter'])

        def configure_urls(self, cls, urls):
            urls += patterns('',
                            (r'^', include('hotel.urls')),
                            (r'^newsletter/', include('newsletter.urls')))
