#!/usr/bin/python
from django.conf import settings
from django.contrib import admin

#from apiclient.discovery import build
from django.contrib.admin.options import InlineModelAdmin


class TranslatableModelMixin(object):

    def translation_list(self):

        if not hasattr(self, 'translations'):
            return {}

        has_trans = []
        for trans in self.translations.all():
            has_trans.append(trans.lang)

        return ', '.join(has_trans)

    translation_list.short_description = 'Translated languages'

class InlineTranslations(object):
    def has_add_permission(self, request):
        return False

    # def has_delete_permission(self, request, obj=None):
    #     return False

class TranslatableAdmin(admin.ModelAdmin):

    # init_translations = []
    # translation_class = None
    # inline_class = None

    # def __init__(self, model, admin_site):
    #
    #     self._TransInlineAdmin = type("%sTranslationInline" % self.translation_class.__name__, (InlineTranslations, self.inline_class), {
    #         'model': self.translation_class,
    #         'suit_classes': 'suit-tab suit-tab-translations'
    #     })
    #
    #     super(TranslatableAdmin, self).__init__(model, admin_site)
    #
    # def get_inline_instances(self, request, obj=None):
    #
    #     if not self.inlines:
    #         self.inlines = []
    #
    #     if not self._TransInlineAdmin in self.inlines:
    #         self.inlines.append(self._TransInlineAdmin)
    #
    #     return super(TranslatableAdmin, self).get_inline_instances(request, obj)
    #
    # def save_model(self, request, obj, form, change):
    #     super(TranslatableAdmin, self).save_model(request, obj, form, change)
    #     self.fix_translations(obj)
    #
    #
    # def fix_translations(self, obj):
    #
    #     if not self.translation_class:
    #         return
    #
    #     has_trans = {}
    #     if hasattr(obj, 'translations'):
    #         for trans in obj.translations.all():
    #             has_trans[trans.lang] = True
    #
    #     for (lang, lang_name) in settings.LANGUAGES:
    #         if lang != settings.MAIN_LANGUAGE and not lang in has_trans:
    #
    #             fields = {'lang': lang}
    #
    #             for field_name in self.init_translations:
    #                 fields[field_name] = translate(getattr(obj, field_name), settings.MAIN_LANGUAGE, lang)
    #
    #             obj.translations.add(self.translation_class(**fields))
    #
    # def get_object(self, request, object_id):
    #     obj = super(TranslatableAdmin, self).get_object(request, object_id)
    #
    #     self.fix_translations(obj)
    #
    #     return obj
    pass

class HiddenTransInline(InlineModelAdmin):
    template = 'admin/trans_inline.html'

class HiddenTransInline(InlineModelAdmin):
    template = 'admin/trans_inline.html'

def TransAdmin(cls, inline_class=admin.StackedInline):

    return TranslatableAdmin
    # return type("%sTranslationAdmin" % cls.__name__, (TranslatableAdmin,), {
    #     'translation_class': cls,
    #     'inline_class': inline_class
    # })

import copy
from django.db import models

def copy_field(f):
    fp = copy.copy(f)

    fp.creation_counter = models.Field.creation_counter
    models.Field.creation_counter += 1

    fp.null = True
    fp.blank = True

    if hasattr(f, "model"):
        del fp.attname
        del fp.column
        del fp.model

        # you may set .name and .verbose_name to None here

    return fp

def translate_model(cls, fields=None):

    model_fields = {
        'lang': models.CharField(max_length=2, editable=False),
        'related': models.ForeignKey(cls, related_name='translations', on_delete=models.PROTECT)
    }

    for target_field_name in fields:
        target_field = cls._meta.get_field_by_name(target_field_name)[0]

        model_fields[target_field_name] = copy_field(target_field)

    model_fields["__module__"] = cls.__module__
    model_fields["__unicode__"] = lambda self: self.lang

    NewModel = type("%sTranslation" % cls.__name__, (models.Model,), model_fields)

    cls.get_translation_class = staticmethod(lambda: NewModel)

    return NewModel



class TranslationError(Exception):
    pass

def translate(message, lang_from, lang_to):

    if lang_to not in _languages:
        raise TranslationError, "Language %s is not supported as lang_to." % lang_to
    if lang_from not in _languages and lang_from != '':
        raise TranslationError, "Language %s is not supported as lang_from." % lang_from

    #try:
    #    service = build('translate', 'v2',
    #                    developerKey='AIzaSyBUKnwQMI3r2pIdSxBAjUjmmrKHoHo1q4s')
    #    data = service.translations().list(
    #        source=lang_from,
    #        target=lang_to,
    #        q=[message]
    #    ).execute()
    #
    #    return data['translations'][0]['translatedText']
    #
    #except Exception, e:
    return None



    # detect_url = "http://ajax.googleapis.com/ajax/services/language/detect?v=1.0&q=%(message)s"

_languages = {
  'af': 'Afrikaans',
  'sq': 'Albanian',
  'am': 'Amharic',
  'ar': 'Arabic',
  'hy': 'Armenian',
  'az': 'Azerbaijani',
  'eu': 'Basque',
  'be': 'Belarusian',
  'bn': 'Bengali',
  'bh': 'Bihari',
  'bg': 'Bulgarian',
  'my': 'Burmese',
  'ca': 'Catalan',
  'chr': 'Cherokee',
  'zh': 'Chinese',
  'zh-CN': 'Chinese_simplified',
  'zh-TW': 'Chinese_traditional',
  'hr': 'Croatian',
  'cs': 'Czech',
  'da': 'Danish',
  'dv': 'Dhivehi',
  'nl': 'Dutch',
  'en': 'English',
  'eo': 'Esperanto',
  'et': 'Estonian',
  'fl': 'Filipino',
  'fi': 'Finnish',
  'fr': 'French',
  'gl': 'Galician',
  'ka': 'Georgian',
  'de': 'German',
  'el': 'Greek',
  'gn': 'Guarani',
  'gu': 'Gujarati',
  'iw': 'Hebrew',
  'hi': 'Hindi',
  'hu': 'Hungarian',
  'is': 'Icelandic',
  'id': 'Indonesian',
  'iu': 'Inuktitut',
  'ga': 'Irish',
  'it': 'Italian',
  'ja': 'Japanese',
  'kn': 'Kannada',
  'kk': 'Kazakh',
  'km': 'Khmer',
  'ko': 'Korean',
  'ku': 'Kurdish',
  'ky': 'Kyrgyz',
  'lo': 'Laothian',
  'lv': 'Latvian',
  'lt': 'Lithuanian',
  'mk': 'Macedonian',
  'ms': 'Malay',
  'ml': 'Malayalam',
  'mt': 'Maltese',
  'mr': 'Marathi',
  'mn': 'Mongolian',
  'ne': 'Nepali',
  'no': 'Norwegian',
  'or': 'Oriya',
  'ps': 'Pashto',
  'fa': 'Persian',
  'pl': 'Polish',
  'pt-PT': 'Portuguese',
  'pa': 'Punjabi',
  'ro': 'Romanian',
  'ru': 'Russian',
  'sa': 'Sanskrit',
  'sr': 'Serbian',
  'sd': 'Sindhi',
  'si': 'Sinhalese',
  'sk': 'Slovak',
  'sl': 'Slovenian',
  'es': 'Spanish',
  'sw': 'Swahili',
  'sv': 'Swedish',
  'tg': 'Tajik',
  'ta': 'Tamil',
  'tl': 'Tagalog',
  'te': 'Telugu',
  'th': 'Thai',
  'bo': 'Tibetan',
  'tr': 'Turkish',
  'uk': 'Ukrainian',
  'ur': 'Urdu',
  'uz': 'Uzbek',
  'ug': 'Uighur',
  'vi': 'Vietnamese',
  'cy': 'Welsh',
  'yi': 'Yiddish'
}

