redis==2.9.1
celery-with-redis==3.0

#inject
#django-imperavi==0.2.3
#django-extensions==1.3.7
#django-newsletter==0.5.2
#django-tinymce
#reportlab==3.1.8
#django-ace==1.0.0
#django-chosen
#django-suit-redactor==0.0.2
#django-paypal==0.1.3
#django-redis-cache==0.11.1
#hiredis==0.1.3
#django-redis-sessions

#uwsgi
#djangocms-text-ckeditor==1.0.11

#django-livesettings
#django-keyedcache
#django-countries

#django-autoslug==1.7.2

#django-jfu
#newrelic

#-e cratis-features

#django_compressor
#BeautifulSoup
#html5lib
#slimit
#lxml
#django-appconf

#raven
#M2Crypto==0.22.3
#opbeat
#johnny-cache



