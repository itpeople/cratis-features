from cratis.features import Feature

# from cms.appresolver import applications_page_check

# -*- coding: utf-8 -*-
"""
Edit Toolbar middleware
"""
#

class ToolbarMiddleware(object):
    """
    Middleware to set up CMS Toolbar.
    """

    def process_request(self, request):
        from cms.cms_toolbar import CMSToolbar
        """
        If we should show the toolbar for this request, put it on
        request.toolbar. Then call the request_hook on the toolbar.
        """
        if 'edit' in request.GET and not request.session.get('cms_edit', False):
            request.session['cms_edit'] = True
        request.toolbar = CMSToolbar(request)

    def process_view(self, request, view_func, view_args, view_kwarg):
        from django.http import HttpResponse
        response = request.toolbar.request_hook()
        if isinstance(response, HttpResponse):
            return response




class LazyPage(object):
   def __get__(self, request, obj_type=None):
       from cms.appresolver import applications_page_check
       from cms.utils.page_resolver import get_page_from_request

       if not hasattr(request, '_current_page_cache'):
           request._current_page_cache = get_page_from_request(request)
           if not request._current_page_cache:
               # if this is in a apphook
               # find the page the apphook is attached to
               request._current_page_cache = applications_page_check(request)
       return request._current_page_cache


class CurrentPageMiddleware(object):
   def process_request(self, request):
       request.__class__.current_page = LazyPage()
       return None


class Cms(Feature):

    def configure_settings(self):
        self.append_apps([
            'djangocms_text_ckeditor',
            'cms',
            'mptt',
            'menus',
            'sekizai',

            # 'cms.plugins.flash',
            'cms.plugins.googlemap',
            'cms.plugins.link',
            # 'cms.plugins.text',
            # 'cms.plugins.twitter'
        ])

        self.append_middleware([
            'cratis_cms.features.CurrentPageMiddleware',
            'cratis_cms.features.ToolbarMiddleware',
            # 'cms.middleware.user.CurrentUserMiddleware',
            # 'cms.middleware.toolbar.ToolbarMiddleware',
            # 'cms.middleware.language.LanguageCookieMiddleware'
        ])

        self.append_template_processor([
            'cms.context_processors.media',
            'sekizai.context_processors.sekizai',
            'django.core.context_processors.request'
        ])

        if not hasattr(self.settings, 'CMS_TEMPLATES'):
            self.settings.CMS_TEMPLATES = ()


    def configure_urls(self, urls):

        from django.conf.urls import url, patterns
        from cms.views import details
        from django.views.decorators.cache import cache_page

        from cratis_i18n.utils import localize_url as _

        if self.settings.DEBUG:
            urls += patterns('',
                             url(_(r'^$'), details, {'slug': ''}, name='pages-root'),
                             url(_(r'^(?P<slug>[0-9A-Za-z-_.//]+)$'), details, name='pages-details-by-slug'),
            )
        else:
            urls += patterns('',
                             url(_(r'^$'), cache_page(60 * 24)(details), {'slug': ''}, name='pages-root'),
                             url(_(r'^(?P<slug>[0-9A-Za-z-_.//]+)$'), cache_page(60 * 24)(details),
                                 name='pages-details-by-slug'),
            )
