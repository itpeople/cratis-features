from caching.base import CachingMixin, CachingManager
from django.db import models

#
# class CachedModel(CachingMixin, models.Model):
#     objects = CachingManager()
#
#     class Meta:
#         abstract = True

class CachedModel(models.Model):
    # objects = CachingManager()

    class Meta:
        abstract = True
