from cratis_ads.models import BannerImage, Banner
from django.contrib import admin


class InlineBannerImage(admin.StackedInline):
    model = BannerImage
    extra = 3
    classes = ['collapse']

    fields = ('title', 'image', 'regions', 'text', 'url')

class BannerAdmin(admin.ModelAdmin):
    inlines = [InlineBannerImage]
    list_display = ('name',)

    class Media:
        css = {
            "all": ('bower/chosen_v1.1.0/chosen.min.css',)
        }
        js = (
            "bower/chosen_v1.1.0/chosen.jquery.min.js",
            "js/admin.js",
        )


admin.site.register(Banner, BannerAdmin)
