
from modeltranslation.translator import translator, TranslationOptions
from cratis_shop_products.models import ProductAttributeType, Product, ProductCategory


class ProductAttributeTypeTranslationOptions(TranslationOptions):
    fields = ('name',)

translator.register(ProductAttributeType, ProductAttributeTypeTranslationOptions)

class ProductTranslationOptions(TranslationOptions):
    fields = ('title', 'long_title', 'slug', 'description', 'long_description')

translator.register(Product, ProductTranslationOptions)

class ProductCategoryTranslationOptions(TranslationOptions):
    fields = ('name', 'description', 'slug')

translator.register(ProductCategory, ProductCategoryTranslationOptions)

