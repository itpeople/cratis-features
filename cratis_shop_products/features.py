from django.conf.urls import patterns
from cratis.features import Feature


class Products(Feature):
    def configure_settings(self):
        super(Products, self).configure_settings()

        self.append_apps([
            "cratis_shop_products",
        ])


